<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Person;

/**
 * PersonsSearch represents the model behind the search form about `app\models\Persons`.
 */
class PersonSearch extends Person
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['persons_id', 'coach_id', 'city_id', 'place_birthday'], 'integer'],
            [['about', 'begin_carier', 'end_carier', 'social_accounts', 'body_type', 'personal_life', 'phones', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Person::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'begin_carier' => $this->begin_carier,
            'end_carier' => $this->end_carier,
            'place_birthday' => $this->place_birthday,
        ]);

        $query->andFilterWhere(['like', 'about', $this->about])
            ->andFilterWhere(['like', 'social_accounts', $this->social_accounts])
            ->andFilterWhere(['like', 'body_type', $this->body_type])
            ->andFilterWhere(['like', 'personal_life', $this->personal_life])
            ->andFilterWhere(['like', 'phones', $this->phones])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

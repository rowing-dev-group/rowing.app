<?php

namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\filters\VerbFilter;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Races".
 *
 * @property integer $race_id
 * @property string $description
 * @property integer $distance
 * @property string $start_date
 * @property integer $heat
 * @property string $progression
 * @property string $weather
 * @property string $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $judge_id
 * @property integer $competition_id
 *
 * @property CrewsRaces[] $crewsRaces
 * @property Crews[] $crews
 * @property Competitions $competition
 * @property User $createdBy
 * @property User $updatedBy
 * @property Persons $judge
 */
class Race extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Races';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'timstampbehavior' => [
                'class' => TimestampBehavior::classname(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameablebehavior' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'many-to-many-bahavior' => [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'crews_ids' => [
                        'crews',
                        'viaTableValues' => [
                            'created_at' => function() {
                                return new \yii\db\Expression('NOW()');
                            },
                            'updated_at' => function() {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => Yii::$app->user->identity->id,
                            'updated_by' => Yii::$app->user->identity->id,
                        ],
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'weather', 'status'], 'string'],
            [['distance', 'heat', 'progression'], 'required'],
            [['distance', 'heat', 'created_by', 'updated_by', 'judge_id', 'competition_id'], 'integer'],
            [['start_date', 'created_at', 'updated_at'], 'safe'],
            [['progression'], 'string', 'max' => 1024],
            [['competition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Competition::className(), 'targetAttribute' => ['competition_id' => 'competition_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['judge_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::className(), 'targetAttribute' => ['judge_id' => 'persons_id']],
            [['crews_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => 'Description',
            'distance' => 'Distance',
            'start_date' => 'Start Date',
            'heat' => 'Heat',
            'progression' => 'Progression',
            'weather' => 'Weather',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * return array of id's
     */
    public static function listAll($keyField = 'race_id', $valueField = 'distance', $asArray = true)
    {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsRaces()
    {
        return $this->hasMany(CrewRace::className(), ['race_id' => 'race_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrews()
    {
        return $this->hasMany(Crew::className(), ['crews_id' => 'crews_id'])->viaTable('Crews_races', ['race_id' => 'race_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompetition()
    {
        return $this->hasOne(Competition::className(), ['competition_id' => 'competition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudge()
    {
        return $this->hasOne(Person::className(), ['persons_id' => 'judge_id']);
    }
}

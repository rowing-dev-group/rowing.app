<?php

namespace common\models;

use Yii;
use yii\filters\VerbFilter;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Boats".
 *
 * @property integer $boats_id
 * @property string $manufacturer
 * @property string $category
 * @property string $create_date
 * @property integer $weigth
 * @property integer $length
 * @property string $state
 * @property string $bow_type
 * @property string $details
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $reiggers
 * @property integer $base_id
 *
 * @property Bases $base
 * @property User $createdBy
 * @property User $updatedBy
 * @property CrewsBoats[] $crewsBoats
 * @property Crews[] $crews
 */
class Boat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Boats';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'timstampbehavior' => [
                'class' => TimestampBehavior::classname(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameablebehavior' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'many-to-many-bahavior' => [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'crews_ids' => 'crews'
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer'], 'required'],
            [['category', 'state', 'bow_type', 'reiggers'], 'string'],
            [['create_date', 'created_at', 'updated_at'], 'safe'],
            [['weigth', 'length', 'created_by', 'updated_by', 'base_id'], 'integer'],
            [['manufacturer'], 'string', 'max' => 255],
            [['details'], 'string', 'max' => 4096],
            [['base_id'], 'exist', 'skipOnError' => true, 'targetClass' => Base::className(), 'targetAttribute' => ['base_id' => 'base_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['crews_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manufacturer' => 'Manufacturer',
            'category' => 'Category',
            'create_date' => 'Create Date',
            'weigth' => 'Weigth',
            'length' => 'Length',
            'state' => 'State',
            'bow_type' => 'Bow Type',
            'details' => 'Details',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'reiggers' => 'Reiggers',
        ];
    }

    /**
     * return array of id's
     */
    public static function listAll($keyField = 'boats_id', $valueField = 'manufacturer', $asArray = true)
    {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBase()
    {
        return $this->hasOne(Base::className(), ['base_id' => 'base_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsBoats()
    {
        return $this->hasMany(CrewBoat::className(), ['boats_id' => 'boats_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrews()
    {
        return $this->hasMany(Crew::className(), ['crews_id' => 'crews_id'])->viaTable('Crews_boats', ['boats_id' => 'boats_id']);
    }
}

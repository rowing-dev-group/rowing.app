<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Training_results".
 *
 * @property integer $training_results_id
 * @property integer $results_type_id
 * @property integer $training_id
 *
 * @property ResultsFields[] $resultsFields
 * @property ResultsType $resultsType
 * @property Training $training
 */
class TrainingResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Training_results';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['results_type_id', 'training_id'], 'required'],
            [['results_type_id', 'training_id'], 'integer'],
            [['results_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ResultType::className(), 'targetAttribute' => ['results_type_id' => 'results_type_id']],
            [['training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['training_id' => 'training_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'training_results_id' => Yii::t('app', 'Training Results ID'),
            'results_type_id' => Yii::t('app', 'Results Type ID'),
            'training_id' => Yii::t('app', 'Training ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultsFields()
    {
        return $this->hasMany(ResultField::className(), ['training_results_id' => 'training_results_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResultsType()
    {
        return $this->hasOne(ResultType::className(), ['results_type_id' => 'results_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(Training::className(), ['training_id' => 'training_id']);
    }
}

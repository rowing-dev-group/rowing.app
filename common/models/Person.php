<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Behavior;

/**
 * This is the model class for table "Persons".
 *
 * @property integer $persons_id
 * @property integer $coach_id
 * @property string $about
 * @property integer $city_id
 * @property string $begin_carier
 * @property string $end_carier
 * @property string $social_accounts
 * @property string $body_type
 * @property string $personal_life
 * @property integer $place_birthday
 * @property string $phones
 * @property string $status
 *
 * @property BasePersons[] $basePersons
 * @property Bases[] $bases
 * @property CrewsPersons[] $crewsPersons
 * @property Crews[] $crews
 * @property City $city
 * @property City $placeBirthday
 * @property PersonsTraining[] $personsTrainings
 * @property Training[] $trainings
 * @property PersonsTypePersons[] $personsTypePersons
 * @property PersonsType[] $personsTypes
 * @property Races[] $races
 * @property User[] $user
 */
class Person extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Persons';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'many-to-many-bahavior' => [
                    'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                    'relations' => [
                        'bases_ids' => 'bases'
                    ],
                ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coach_id', 'city_id', 'place_birthday'], 'integer'],
            [['city_id', 'begin_carier'], 'required'],
            [['begin_carier', 'end_carier'], 'safe'],
            [['body_type', 'status'], 'string'],
            [['about'], 'string', 'max' => 4096],
            [['social_accounts'], 'string', 'max' => 5120],
            [['personal_life'], 'string', 'max' => 2048],
            [['phones'], 'string', 'max' => 3072],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'city_id']],
            [['place_birthday'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['place_birthday' => 'city_id']],
            [['bases_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'about' => 'About',
            'begin_carier' => 'Begin Carier',
            'end_carier' => 'End Carier',
            'body_type' => 'Body Type',
            'personal_life' => 'Personal Life',
            'place_birthday' => 'Place Birthday',
            'phones' => 'Phones',
            'status' => 'Status',
        ];
    }

    /**
     * return array of id's
     */
    public static function listAll($keyField = 'persons_id', $valueField = 'social_accounts', $asArray = true)
    {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasePersons()
    {
        return $this->hasMany(BasePersons::className(), ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBases()
    {
        return $this->hasMany(Base::className(), ['base_id' => 'base_id'])->viaTable('Base_persons', ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsPersons()
    {
        return $this->hasMany(CrewsPersons::className(), ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrews()
    {
        return $this->hasMany(Crew::className(), ['crews_id' => 'crews_id'])->viaTable('Crews_persons', ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaceBirthday()
    {
        return $this->hasOne(City::className(), ['city_id' => 'place_birthday']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonsTrainings()
    {
        return $this->hasMany(PersonsTraining::className(), ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(Training::className(), ['training_id' => 'training_id'])->viaTable('Persons_training', ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonsTypePersons()
    {
        return $this->hasMany(PersonsTypePersons::className(), ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonsTypes()
    {
        return $this->hasMany(PersonType::className(), ['persons_type_id' => 'persons_type_id'])->viaTable('Persons_type_persons', ['persons_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaces()
    {
        return $this->hasMany(Race::className(), ['judge_id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasMany(User::className(), ['persons_id' => 'persons_id']);
    }
}

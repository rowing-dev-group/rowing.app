<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Race;

/**
 * RacesSearch represents the model behind the search form about `app\models\Races`.
 */
class RaceSearch extends Race
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['race_id', 'distance', 'heat', 'created_by', 'updated_by', 'judge_id', 'competition_id'], 'integer'],
            [['description', 'start_date', 'progression', 'weather', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Race::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'distance' => $this->distance,
            'start_date' => $this->start_date,
            'heat' => $this->heat,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'progression', $this->progression])
            ->andFilterWhere(['like', 'weather', $this->weather])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

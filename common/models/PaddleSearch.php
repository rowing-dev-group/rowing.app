<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Paddle;

/**
 * PaddlesSearch represents the model behind the search form about `app\models\Paddles`.
 */
class PaddleSearch extends Paddle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paddles_id', 'weigth', 'length', 'created_by', 'updated_by', 'base_id'], 'integer'],
            [['paddle', 'create_date', 'state', 'type_paddle', 'details', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Paddle::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'paddles_id' => $this->paddles_id,
            'create_date' => $this->create_date,
            'weigth' => $this->weigth,
            'length' => $this->length,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'base_id' => $this->base_id,
        ]);

        $query->andFilterWhere(['like', 'paddle', $this->paddle])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'type_paddle', $this->type_paddle])
            ->andFilterWhere(['like', 'details', $this->details]);

        return $dataProvider;
    }
}

<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Base;

/**
 * BaseSearch represents the model behind the search form about `app\models\Base`.
 */
class BaseSearch extends Base
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['base_id', 'created_by', 'updated_by', 'city_id', 'water_id', 'director'], 'integer'],
            [['name', 'created_at', 'updated_at', 'address', 'description', 'phones', 'email', 'work_time', 'date_founding'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Base::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'base_id' => $this->base_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'city_id' => $this->city_id,
            'water_id' => $this->water_id,
            'date_founding' => $this->date_founding,
            'director' => $this->director,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'phones', $this->phones])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'work_time', $this->work_time]);

        return $dataProvider;
    }
}

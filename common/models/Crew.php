<?php

namespace common\models;

use Yii;
use yii\filters\VerbFilter;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Crews".
 *
 * @property integer $crews_id
 * @property string $slogan
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $crew_type
 * @property string $status
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property CrewsBoats[] $crewsBoats
 * @property Boats[] $boats
 * @property CrewsPaddles[] $crewsPaddles
 * @property Paddles[] $paddles
 * @property CrewsPersons[] $crewsPersons
 * @property Persons[] $persons
 * @property CrewsRaces[] $crewsRaces
 * @property Races[] $races
 */
class Crew extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Crews';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'timstampbehavior' => [
                'class' => TimestampBehavior::classname(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameablebehavior' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'many-to-many-bahavior' => [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'persons_ids' => 'persons',
                    'paddles_ids' => 'paddles',
                    'boats_ids' => 'boats',
                    'race_ids' => [
                        'races',
                        'viaTableValues' => [
                            'created_at' => function() {
                                return new \yii\db\Expression('NOW()');
                            },
                            'updated_at' => function() {
                                return new \yii\db\Expression('NOW()');
                            },
                            'created_by' => Yii::$app->user->identity->id,
                            'updated_by' => Yii::$app->user->identity->id,
                        ],
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['crew_type', 'status'], 'string'],
            [['slogan'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['race_ids', 'persons_ids', 'paddles_ids', 'boats_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'slogan' => 'Slogan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'crew_type' => 'Crew Type',
            'status' => 'Status',
        ];
    }

    /**
     * return array of id's
     */
    public static function listAll($keyField = 'crews_id', $valueField = 'slogan', $asArray = true)
    {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsBoats()
    {
        return $this->hasMany(CrewsBoats::className(), ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoats()
    {
        return $this->hasMany(Boat::className(), ['boats_id' => 'boats_id'])->viaTable('Crews_boats', ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsPaddles()
    {
        return $this->hasMany(CrewsPaddles::className(), ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaddles()
    {
        return $this->hasMany(Paddle::className(), ['paddles_id' => 'paddles_id'])->viaTable('Crews_paddles', ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsPersons()
    {
        return $this->hasMany(CrewsPersons::className(), ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersons()
    {
        return $this->hasMany(Person::className(), ['persons_id' => 'persons_id'])->viaTable('Crews_persons', ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsRaces()
    {
        return $this->hasMany(CrewRace::className(), ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRaces()
    {
        return $this->hasMany(Race::className(), ['race_id' => 'race_id'])->viaTable('Crews_races', ['crews_id' => 'crews_id']);
    }
}

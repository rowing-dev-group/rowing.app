<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Boat;

/**
 * BoatsSearch represents the model behind the search form about `app\models\Boat`.
 */
class BoatSearch extends Boat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['boats_id', 'weigth', 'length', 'created_by', 'updated_by', 'base_id'], 'integer'],
            [['manufacturer', 'category', 'create_date', 'state', 'bow_type', 'details', 'created_at', 'updated_at', 'reiggers'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Boat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'boats_id' => $this->boats_id,
            'create_date' => $this->create_date,
            'weigth' => $this->weigth,
            'length' => $this->length,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'base_id' => $this->base_id,
        ]);

        $query->andFilterWhere(['like', 'manufacturer', $this->manufacturer])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'bow_type', $this->bow_type])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'reiggers', $this->reiggers]);

        return $dataProvider;
    }
}

<?php

namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\filters\VerbFilter;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Paddles".
 *
 * @property integer $paddles_id
 * @property string $paddle
 * @property string $create_date
 * @property integer $weigth
 * @property integer $length
 * @property string $state
 * @property string $type_paddle
 * @property string $details
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $base_id
 *
 * @property CrewsPaddles[] $crewsPaddles
 * @property Crews[] $crews
 * @property Bases $base
 * @property User $createdBy
 * @property User $updatedBy
 */
class Paddle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Paddles';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'timstampbehavior' => [
                'class' => TimestampBehavior::classname(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameablebehavior' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'many-to-many-bahavior' => [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'crews_ids' => 'crews'
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['paddle'], 'required'],
            [['create_date', 'created_at', 'updated_at'], 'safe'],
            [['weigth', 'length', 'created_by', 'updated_by', 'base_id'], 'integer'],
            [['state', 'type_paddle'], 'string'],
            [['paddle'], 'string', 'max' => 255],
            [['details'], 'string', 'max' => 4096],
            [['base_id'], 'exist', 'skipOnError' => true, 'targetClass' => Base::className(), 'targetAttribute' => ['base_id' => 'base_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['crews_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'paddle' => 'Paddle',
            'create_date' => 'Create Date',
            'weigth' => 'Weigth',
            'length' => 'Length',
            'state' => 'State',
            'type_paddle' => 'Type Paddle',
            'details' => 'Details',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * return array of id's
     */
    public static function listAll($keyField = 'paddles_id', $valueField = 'paddle', $asArray = true)
    {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrewsPaddles()
    {
        return $this->hasMany(CrewPaddle::className(), ['paddles_id' => 'paddles_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrews()
    {
        return $this->hasMany(Crew::className(), ['crews_id' => 'crews_id'])->viaTable('Crews_paddles', ['paddles_id' => 'paddles_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBase()
    {
        return $this->hasOne(Base::className(), ['base_id' => 'base_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }
}

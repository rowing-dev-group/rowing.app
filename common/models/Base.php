<?php

namespace common\models;

use Yii;
use yii\filters\VerbFilter;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Bases".
 *
 * @property integer $base_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $address
 * @property integer $city_id
 * @property integer $water_id
 * @property string $description
 * @property string $phones
 * @property string $email
 * @property string $work_time
 * @property string $date_founding
 * @property integer $director
 *
 * @property BasesPersons[] $basesPersons
 * @property Persons[] $persons
 * @property User $createdBy
 * @property User $updatedBy
 * @property Boats[] $boats
 * @property Paddles[] $paddles
 */
class Base extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Bases';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'timstampbehavior' => [
                'class' => TimestampBehavior::classname(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameablebehavior' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'many-to-many-bahavior' => [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'persons_ids' => 'persons'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'city_id', 'email', 'work_time', 'date_founding'], 'required'],
            [['created_at', 'updated_at', 'date_founding'], 'safe'],
            [['created_by', 'updated_by', 'city_id', 'water_id', 'director'], 'integer'],
            [['name', 'work_time'], 'string', 'max' => 1024],
            [['address', 'description'], 'string', 'max' => 4096],
            [['phones'], 'string', 'max' => 3072],
            [['email'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
            [['persons_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'address' => 'Address',
            'description' => 'Description',
            'phones' => 'Phones',
            'email' => 'Email',
            'work_time' => 'Work Time',
            'date_founding' => 'Date Founding',
            'director' => 'Director',
            'persons_ids' => 'Persons',
        ];
    }

    /**
     * return array of id's
     */
    public static function listAll($keyField = 'base_id', $valueField = 'name', $asArray = true)
    {
        $query = static::find();
        if ($asArray) {
            $query->select([$keyField, $valueField])->asArray();
        }

        return ArrayHelper::map($query->all(), $keyField, $valueField);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasesPersons()
    {
        return $this->hasMany(BasesPersons::className(), ['base_id' => 'base_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersons()
    {
        return $this->hasMany(Person::className(), ['persons_id' => 'persons_id'])->viaTable('Base_persons', ['base_id' => 'base_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBoats()
    {
        return $this->hasMany(Boat::className(), ['base_id' => 'base_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaddles()
    {
        return $this->hasMany(Paddle::className(), ['base_id' => 'base_id']);
    }
}

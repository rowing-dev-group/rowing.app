<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Crew;

/**
 * CrewsSearch represents the model behind the search form about `app\models\Crews`.
 */
class CrewSearch extends Crew
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crews_id', 'created_by', 'updated_by'], 'integer'],
            [['slogan', 'created_at', 'updated_at', 'crew_type', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Crew::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'crews_id' => $this->crews_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'slogan', $this->slogan])
            ->andFilterWhere(['like', 'crew_type', $this->crew_type])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php

namespace common\models;

use Yii;
use yii\base\Behavior;
use yii\filters\VerbFilter;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "Results_fields".
 *
 * @property integer $results_fields_id
 * @property double $value
 * @property string $description
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $fields_type_id
 * @property integer $training_results_id
 *
 * @property FieldsType $fieldsType
 * @property TrainingResults $trainingResults
 * @property User $createdBy
 * @property User $updatedBy
 */
class ResultField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Results_fields';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'timstampbehavior' => [
                'class' => TimestampBehavior::classname(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameablebehavior' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'fields_type_id', 'training_results_id'], 'required'],
            [['value'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'fields_type_id', 'training_results_id'], 'integer'],
            [['description'], 'string', 'max' => 4096],
            [['fields_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FieldType::className(), 'targetAttribute' => ['fields_type_id' => 'fields_type_id']],
            [['training_results_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingResult::className(), 'targetAttribute' => ['training_results_id' => 'training_results_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'value' => 'Value',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFieldsType()
    {
        return $this->hasOne(FieldType::className(), ['fields_type_id' => 'fields_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingResults()
    {
        return $this->hasOne(TrainingResults::className(), ['training_results_id' => 'training_results_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }
}

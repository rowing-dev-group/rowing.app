<?php

namespace common\models;

use Yii;
use yii\filters\VerbFilter;
use yii\base\Behavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "Crews_races".
 *
 * @property integer $crews_id
 * @property integer $race_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $line_number
 * @property integer $time_2000
 * @property integer $time_500
 * @property integer $time_1000
 * @property integer $time_1500
 * @property integer $temp_500
 * @property integer $temp_1000
 * @property integer $temp_1500
 * @property integer $temp_2000
 * @property integer $temp_avg
 * @property integer $position_500
 * @property integer $position_1000
 * @property integer $position_1500
 * @property integer $position_2000
 *
 * @property Crews $crews
 * @property Races $race
 * @property User $createdBy
 * @property User $updatedBy
 */
class CrewRace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Crews_races';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crews_id', 'race_id', 'line_number', 'time_2000', 'position_2000'], 'required'],
            [['crews_id', 'race_id', 'created_by', 'updated_by', 'line_number', 'time_2000', 'time_500', 'time_1000', 'time_1500', 'temp_500', 'temp_1000', 'temp_1500', 'temp_2000', 'temp_avg', 'position_500', 'position_1000', 'position_1500', 'position_2000'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['crews_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crew::className(), 'targetAttribute' => ['crews_id' => 'crews_id']],
            [['race_id'], 'exist', 'skipOnError' => true, 'targetClass' => Race::className(), 'targetAttribute' => ['race_id' => 'race_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'timstampbehavior' => [
                'class' => TimestampBehavior::classname(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameablebehavior' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'race_id' => 'Race ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'line_number' => 'Line Number',
            'time_2000' => 'Time 2000',
            'time_500' => 'Time 500',
            'time_1000' => 'Time 1000',
            'time_1500' => 'Time 1500',
            'temp_500' => 'Temp 500',
            'temp_1000' => 'Temp 1000',
            'temp_1500' => 'Temp 1500',
            'temp_2000' => 'Temp 2000',
            'temp_avg' => 'Temp Avg',
            'position_500' => 'Position 500',
            'position_1000' => 'Position 1000',
            'position_1500' => 'Position 1500',
            'position_2000' => 'Position 2000',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrews()
    {
        return $this->hasOne(Crew::className(), ['crews_id' => 'crews_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRace()
    {
        return $this->hasOne(Race::className(), ['race_id' => 'race_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['user_id' => 'updated_by']);
    }
}

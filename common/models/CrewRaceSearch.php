<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CrewRace;

/**
 * CrewsRacesSearch represents the model behind the search form about `app\models\CrewsRaces`.
 */
class CrewRaceSearch extends CrewRace
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['crews_id', 'race_id', 'created_by', 'updated_by', 'line_number', 'time_2000', 'time_500', 'time_1000', 'time_1500', 'temp_500', 'temp_1000', 'temp_1500', 'temp_2000', 'temp_avg', 'position_500', 'position_1000', 'position_1500', 'position_2000'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CrewRace::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'crews_id' => $this->crews_id,
            'race_id' => $this->race_id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'line_number' => $this->line_number,
            'time_2000' => $this->time_2000,
            'time_500' => $this->time_500,
            'time_1000' => $this->time_1000,
            'time_1500' => $this->time_1500,
            'temp_500' => $this->temp_500,
            'temp_1000' => $this->temp_1000,
            'temp_1500' => $this->temp_1500,
            'temp_2000' => $this->temp_2000,
            'temp_avg' => $this->temp_avg,
            'position_500' => $this->position_500,
            'position_1000' => $this->position_1000,
            'position_1500' => $this->position_1500,
            'position_2000' => $this->position_2000,
        ]);

        return $dataProvider;
    }
}

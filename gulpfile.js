var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var cleanCSS = require('gulp-clean-css');

var paths = {
    files : {
        sass : {
            frontend : './frontend/sass/**/*.scss',
            backend : './backend/sass/**/*.scss',
            common : './common/sass/**/*.scss'
        }
    },
    directories : {
        css : {
            frontend : './frontend/web/css',
            backend : './backend/web/css'
        }
    }
}

gulp.task('sass:debug:frontend', function () {
    return gulp.src(paths.files.sass.frontend)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.directories.css.frontend));
});

gulp.task('sass:debug:backend', function () {
    return gulp.src(paths.files.sass.backend)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.directories.css.backend));
});

gulp.task('sass:build:frontend', function () {
    return gulp.src(paths.files.sass.frontend)
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({ debug: true }, logMinifidInfo))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.directories.css.frontend));
});

gulp.task('sass:build:backend', function () {
    return gulp.src(paths.files.sass.backend)
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({ debug: true }, logMinifidInfo))
        .pipe(plumber.stop())
        .pipe(gulp.dest(paths.directories.css.backend));
});

gulp.task('debug', ['sass:debug:frontend', 'sass:debug:backend'], function () {
    gulp.watch(paths.files.sass.backend, ['sass:debug:backend']);
    gulp.watch(paths.files.sass.frontend, ['sass:debug:frontend']);
    gulp.watch(paths.files.sass.common, ['sass:debug:frontend', 'sass:debug:backend']);
});

gulp.task('build', ['sass:build:backend', 'sass:build:frontend']);

gulp.task('default', ['build']);

function logMinifidInfo(details) {
    console.log('--------------------------------------');
    console.log(' File ', details.name);
    console.log('          Time spent :', details.stats.timeSpent);
    console.log('          Efficiency :', details.stats.efficiency);
    console.log('          Original size : ', details.stats.originalSize);
    console.log('          Minified size : ', details.stats.minifiedSize);
    console.log('--------------------------------------')
}
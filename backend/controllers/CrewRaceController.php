<?php

namespace backend\controllers;

use Yii;
use common\models\CrewRace;
use common\models\CrewRaceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CrewsRacesController implements the CRUD actions for CrewRace model.
 */
class CrewRaceController extends Controller
{
    /**
     * Lists all CrewRace models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CrewRaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CrewRace model.
     * @param integer $crews_id
     * @param integer $race_id
     * @return mixed
     */
    public function actionView($crews_id, $race_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($crews_id, $race_id),
        ]);
    }

    /**
     * Creates a new CrewRace model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CrewRace();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'crews_id' => $model->crews_id, 'race_id' => $model->race_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CrewRace model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $crews_id
     * @param integer $race_id
     * @return mixed
     */
    public function actionUpdate($crews_id, $race_id)
    {
        $model = $this->findModel($crews_id, $race_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'crews_id' => $model->crews_id, 'race_id' => $model->race_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CrewRace model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $crews_id
     * @param integer $race_id
     * @return mixed
     */
    public function actionDelete($crews_id, $race_id)
    {
        $this->findModel($crews_id, $race_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CrewRace model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $crews_id
     * @param integer $race_id
     * @return CrewRace the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($crews_id, $race_id)
    {
        if (($model = CrewRace::findOne(['crews_id' => $crews_id, 'race_id' => $race_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

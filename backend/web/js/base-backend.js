(function(){
	$(document).ready(function(){
		var sidebar = $('#sidebar > .panel');
		sidebar.each(function(){
		    let div	= $(this);
			div.popover({
				content: div.find('.panel-collapse').html(),
				html: true,
				title: div.find('.sidebar-title').text(),
				trigger: 'focus',
				container: 'body',
				toggle: 'popover',
				type: 'button',
				placement: 'right'
			});
		});
		$(window).on('resize load', function(){
			if ($(this).width() > 768) {
				sidebar.popover('disable');
			}
		    else {
		    	sidebar.popover('enable');
			}
		});
	});
})();
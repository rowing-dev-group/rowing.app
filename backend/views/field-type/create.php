<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FieldType */

$this->title = 'Create Fields Type';
$this->params['breadcrumbs'][] = ['label' => 'Fields Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fields-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

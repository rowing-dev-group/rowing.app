<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FieldType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fields-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([ 'weight' => 'Weight', 'length' => 'Length', 'width' => 'Width', 'height' => 'Height', 'distance' => 'Distance', 'time' => 'Time', 'the_temp' => 'The temp', 'pulse' => 'Pulse', 'blood_pressure' => 'Blood pressure', 'watts' => 'Watts', 'calories' => 'Calories', 'the_approach' => 'The approach', 'the_repetition' => 'The repetition', 'the_angle_of_inclination' => 'The angle of inclination', 'lactate' => 'Lactate', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'results_type_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

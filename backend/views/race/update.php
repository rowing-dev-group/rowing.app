<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Race */

$this->title = 'Update Races: ' . $model->race_id;
$this->params['breadcrumbs'][] = ['label' => 'Races', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->race_id, 'url' => ['view', 'id' => $model->race_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="races-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RaceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="races-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'race_id') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'distance') ?>

    <?= $form->field($model, 'start_date') ?>

    <?= $form->field($model, 'heat') ?>

    <?php // echo $form->field($model, 'progression') ?>

    <?php // echo $form->field($model, 'weather') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'judge_id') ?>

    <?php // echo $form->field($model, 'competition_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

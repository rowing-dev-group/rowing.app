<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Crew;

/* @var $this yii\web\View */
/* @var $model app\models\Race */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="races-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'distance')->textInput() ?>

    <?= $form->field($model, 'start_date')->textInput() ?>

    <?= $form->field($model, 'heat')->textInput() ?>

    <?= $form->field($model, 'progression')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weather')->dropDownList([ 'favourable' => 'Favourable', 'headwind' => 'Headwind', 'crosswind' => 'Crosswind', 'calm' => 'Calm', 'storm' => 'Storm', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'published' => 'Published', 'unpublished' => 'Unpublished', 'cancelled' => 'Cancelled', 'waiting' => 'Waiting', 'finished' => 'Finished', 'deleted' => 'Deleted', 'arhive' => 'Arhive', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'judge_id')->textInput() ?>

    <?= $form->field($model, 'crews_ids')->dropDownList(Crew::listAll(), ['multiple' => true]) ?>

    <?= $form->field($model, 'competition_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

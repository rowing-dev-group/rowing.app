<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Race */

$this->title = 'Create Races';
$this->params['breadcrumbs'][] = ['label' => 'Races', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="races-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

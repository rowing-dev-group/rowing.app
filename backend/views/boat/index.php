<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BoatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Boats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boats-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Boats', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'manufacturer',
            'category',
            'create_date',
            'weigth',
            // 'length',
            // 'state',
            // 'bow_type',
            // 'details',
            // 'reiggers',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

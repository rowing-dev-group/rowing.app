<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Boat */

$this->title = 'Update Boats: ' . $model->boats_id;
$this->params['breadcrumbs'][] = ['label' => 'Boats', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->boats_id, 'url' => ['view', 'id' => $model->boats_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="boats-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

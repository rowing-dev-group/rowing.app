<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Boat */

$this->title = $model->boats_id;
$this->params['breadcrumbs'][] = ['label' => 'Boats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="boats-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->boats_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->boats_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'boats_id',
            'manufacturer',
            'category',
            'create_date',
            'weigth',
            'length',
            'state',
            'bow_type',
            'details',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'reiggers',
            'base_id',
        ],
    ]) ?>

</div>

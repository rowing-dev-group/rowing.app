<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Crew;

/* @var $this yii\web\View */
/* @var $model app\models\Boat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="boats-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->dropDownList([ 'LM1x' => 'LM1x', 'LM2-' => 'LM2-', 'LM2x' => 'LM2x', 'LM4-' => 'LM4-', 'M2+' => 'M2+', 'LM4x' => 'LM4x', 'LM8+' => 'LM8+', 'M1x' => 'M1x', 'M2-' => 'M2-', 'M2x' => 'M2x', 'M4-' => 'M4-', 'M4+' => 'M4+', 'M4x' => 'M4x', 'M8+' => 'M8+', 'LW1x' => 'LW1x', 'LW2x' => 'LW2x', 'LW4x' => 'LW4x', 'W1x' => 'W1x', 'W2-' => 'W2-', 'W2x' => 'W2x', 'W4-' => 'W4-', 'W4x' => 'W4x', 'W8+' => 'W8+', 'ASM1x' => 'ASM1x', 'ASW1x' => 'ASW1x', 'LTAMix4+' => 'LTAMix4+', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'weigth')->textInput() ?>

    <?= $form->field($model, 'length')->textInput() ?>

    <?= $form->field($model, 'state')->dropDownList([ 'new' => 'New', 'normal' => 'Normal', 'damage' => 'Damage', 'critical' => 'Critical', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'bow_type')->dropDownList([ 'acute' => 'Acute', 'straigth' => 'Straigth', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reiggers')->dropDownList([ 'carbon' => 'Carbon', 'aluminium' => 'Aluminium', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'base_id')->textInput() ?>

    <?= $form->field($model, 'crews_ids')->dropDownList(Crew::listAll(), ['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
use yii\helpers\Html;
use yii\bootstrap\BaseHtml;
use yii\helpers\Url;
use yii\bootstrap\Collapse;
?>
<?= Collapse::widget([
      'items' => [
          [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Users', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('user', Url::to('user', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs']
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Training', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('Training', Url::to('training', true)),
                Html::a('field-type', Url::to('field-type', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs']
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'News', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('news', Url::to('news', true)),
                Html::a('rubric', Url::to('rubric', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Comments', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('comment', Url::to('comment', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Competitions', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('competition', Url::to('competition', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Races', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('race', Url::to('race', true)),
                Html::a('crew-race', Url::to('crew-race', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Persons', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('person', Url::to('person', true)),
                Html::a('type', Url::to('person-type', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Crews', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('crew', Url::to('crew', true)),
                Html::a('crew-races', Url::to('crew-race', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Results', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('result-fields', Url::to('result-field', true)),
                Html::a('result-type', Url::to('result-type', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Bases', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('base', Url::to('base', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Waters', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('water', Url::to('water', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Paddless', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('paddle', Url::to('paddle', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
        ],
        [
            'label' => BaseHtml::button('*')
                . Html::tag('span', 'Boats', ['class' => 'col-sm-offset-1 col-md-offset-1 col-lg-offset-1 sidebar-title hidden-xs']),
            'content' => [
                Html::a('boats', Url::to('boat', true)),
                Html::a('settings', '#'),
                Html::a('statistics', '#')
            ],
            'contentOptions' => ['class' => 'hidden-xs'],
          ],
      ],
      'encodeLabels' => false,
      'id' => 'sidebar'
]); ?>
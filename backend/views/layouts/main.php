<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Dropdown;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="admin.rowing.app/rowingapp.ico" type="image/x-icon">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
        NavBar::begin(
            [
                'brandLabel' => 'Rowing app',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-default',
                ],
            ]);
        echo Nav::widget([
            'items' => [
                ['label' => 'Home', 'url' => ['/site/home']],
                ['label' => 'Notes', 'url' => ['/site/notes']],
                ['label' => 'Messages', 'url' => ['/site/messages']],
                ['label' => 'Setting', 'url' => ['/site/setting']],

                Html::beginTag('li', ['class' => 'dropdown'])
                . Html::beginTag('a',  ['class' => 'dropdown-toggle',
                                        'data-toggle' => 'dropdown',
                                        'aria-expanded' => true,
                                        'href' => '#'])
                . Yii::$app->user->identity->username
                . Html::img('https://dummyimage.com/30x30/ffffff/000000.png', ['class' => 'img-circle'])
                . '<b class="caret"></b>'
                . html::endTag('a')
                . Dropdown::widget([
                    'items' => [
                        ['label' => 'Profile settings', 'url' => '/'],
                        ['label' => 'Help', 'url' => '/'],
                        Html::beginTag('li')
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::beginTag('div', ['class' => 'btn-group btn-group-justified', 'role' => 'group', 'aria-label' => '...'])
                            . Html::beginTag('div', ['class' => 'btn-group', 'role' => 'group'])
                            . Html::submitButton(
                                Html::tag('span', 'logout', ['class' => 'icon-unlocked']),
                                ['class' => 'btn btn-xs btn-primary']
                            )
                            . Html::endTag('div')
                            . Html::endTag('div')
                            . Html::endForm()
                        . Html::endTag('li')
                    ],
                ])
                .html::endTag('li')
            ],
            'options' => ['class' => 'navbar-nav navbar-right'],
        ]);
        NavBar::end();
        ?>
        <div class="container">
            <div class="row">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],])
                ?>
                <?= Alert::widget() ?>
                <div class="col-xs-2 col-sm-3 col-md-2 col-lg-2">
                    <div class="row">
                        <?=
                        Yii::$app->view->renderFile('@app/views/layouts/sidebar.php');
                        ?>
                    </div>
                </div>
                <div class="col-xs-10 col-sm-9 col-md-10 col-lg-10">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <footer>
            <p class="text-center">&copy  Rowing app <?=date('Y')?></p>
        </footer>
        <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
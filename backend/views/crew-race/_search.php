<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CrewsRacesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crews-races-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'crews_id') ?>

    <?= $form->field($model, 'race_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'created_by') ?>

    <?= $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'line_number') ?>

    <?php // echo $form->field($model, 'time_2000') ?>

    <?php // echo $form->field($model, 'time_500') ?>

    <?php // echo $form->field($model, 'time_1000') ?>

    <?php // echo $form->field($model, 'time_1500') ?>

    <?php // echo $form->field($model, 'temp_500') ?>

    <?php // echo $form->field($model, 'temp_1000') ?>

    <?php // echo $form->field($model, 'temp_1500') ?>

    <?php // echo $form->field($model, 'temp_2000') ?>

    <?php // echo $form->field($model, 'temp_avg') ?>

    <?php // echo $form->field($model, 'position_500') ?>

    <?php // echo $form->field($model, 'position_1000') ?>

    <?php // echo $form->field($model, 'position_1500') ?>

    <?php // echo $form->field($model, 'position_2000') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CrewRace */

$this->title = $model->crews_id;
$this->params['breadcrumbs'][] = ['label' => 'Crews Races', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crews-races-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'crews_id' => $model->crews_id, 'race_id' => $model->race_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'crews_id' => $model->crews_id, 'race_id' => $model->race_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'crews_id',
            'race_id',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'line_number',
            'time_2000:datetime',
            'time_500:datetime',
            'time_1000:datetime',
            'time_1500:datetime',
            'temp_500',
            'temp_1000',
            'temp_1500',
            'temp_2000',
            'temp_avg',
            'position_500',
            'position_1000',
            'position_1500',
            'position_2000',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CrewRaceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Crews Races';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crews-races-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Crews Races', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
             'line_number',
             'time_2000:datetime',
             'time_500:datetime',
             'time_1000:datetime',
             'time_1500:datetime',
             'temp_500',
             'temp_1000',
             'temp_1500',
             'temp_2000',
             'temp_avg',
             'position_500',
             'position_1000',
             'position_1500',
             'position_2000',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

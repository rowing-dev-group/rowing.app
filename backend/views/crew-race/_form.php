<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CrewRace */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crews-races-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'crews_id')->textInput() ?>

    <?= $form->field($model, 'race_id')->textInput() ?>

    <?= $form->field($model, 'line_number')->textInput() ?>

    <?= $form->field($model, 'time_2000')->textInput() ?>

    <?= $form->field($model, 'time_500')->textInput() ?>

    <?= $form->field($model, 'time_1000')->textInput() ?>

    <?= $form->field($model, 'time_1500')->textInput() ?>

    <?= $form->field($model, 'temp_500')->textInput() ?>

    <?= $form->field($model, 'temp_1000')->textInput() ?>

    <?= $form->field($model, 'temp_1500')->textInput() ?>

    <?= $form->field($model, 'temp_2000')->textInput() ?>

    <?= $form->field($model, 'temp_avg')->textInput() ?>

    <?= $form->field($model, 'position_500')->textInput() ?>

    <?= $form->field($model, 'position_1000')->textInput() ?>

    <?= $form->field($model, 'position_1500')->textInput() ?>

    <?= $form->field($model, 'position_2000')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

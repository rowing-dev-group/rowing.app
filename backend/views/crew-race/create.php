<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CrewRace */

$this->title = 'Create Crews Races';
$this->params['breadcrumbs'][] = ['label' => 'Crews Races', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crews-races-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

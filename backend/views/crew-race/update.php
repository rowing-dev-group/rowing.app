<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CrewsRaces */

$this->title = 'Update Crews Races: ' . $model->crews_id;
$this->params['breadcrumbs'][] = ['label' => 'Crews Races', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->crews_id, 'url' => ['view', 'crews_id' => $model->crews_id, 'race_id' => $model->race_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="crews-races-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

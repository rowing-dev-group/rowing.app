<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rubric */

$this->title = 'Update Rubrics: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Rubrics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->rubrics_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rubrics-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

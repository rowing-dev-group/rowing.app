<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Crew */

$this->title = $model->crews_id;
$this->params['breadcrumbs'][] = ['label' => 'Crews', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crews-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->crews_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->crews_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'crews_id',
            'slogan',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'crew_type',
            'status',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Person;
use common\models\Paddle;
use common\models\Boat;
use common\models\Race;
/* @var $this yii\web\View */
/* @var $model app\models\Crew */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="crews-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'slogan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'crew_type')->dropDownList([ 'LM1x' => 'LM1x', 'LM2-' => 'LM2-', 'LM2x' => 'LM2x', 'LM4-' => 'LM4-', 'M2+' => 'M2+', 'LM4x' => 'LM4x', 'LM8+' => 'LM8+', 'M1x' => 'M1x', 'M2-' => 'M2-', 'M2x' => 'M2x', 'M4-' => 'M4-', 'M4+' => 'M4+', 'M4x' => 'M4x', 'M8+' => 'M8+', 'LW1x' => 'LW1x', 'LW2x' => 'LW2x', 'LW4x' => 'LW4x', 'W1x' => 'W1x', 'W2-' => 'W2-', 'W2x' => 'W2x', 'W4-' => 'W4-', 'W4x' => 'W4x', 'W8+' => 'W8+', 'ASM1x' => 'ASM1x', 'ASW1x' => 'ASW1x', 'LTAMix4+' => 'LTAMix4+', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'once_only' => 'Once only', 'active' => 'Active', 'inactive' => 'Inactive', 'awaiting_confirmation' => 'Awaiting confirmation', 'Suspended' => 'Suspended', ], ['prompt' => '']) ?>

	<?= $form->field($model, 'persons_ids')->dropDownList(Person::listAll(), ['multiple' => true]) ?>

	<?= $form->field($model, 'paddles_ids')->dropDownList(Paddle::listAll(), ['multiple' => true]) ?>

    <?= $form->field($model, 'boats_ids')->dropDownList(Boat::listAll(), ['multiple' => true]) ?>

	<?= $form->field($model, 'race_ids')->dropDownList(Race::listAll(), ['multiple' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

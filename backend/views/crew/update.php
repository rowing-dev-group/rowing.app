<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Crew */

$this->title = 'Update Crews: ' . $model->crews_id;
$this->params['breadcrumbs'][] = ['label' => 'Crews', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->crews_id, 'url' => ['view', 'id' => $model->crews_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="crews-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

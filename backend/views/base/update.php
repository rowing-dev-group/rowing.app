<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bases */

$this->title = 'Update Bases: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Bases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->base_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bases-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

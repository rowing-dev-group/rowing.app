<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ResultType */

$this->title = 'Create Results Type';
$this->params['breadcrumbs'][] = ['label' => 'Results Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="results-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

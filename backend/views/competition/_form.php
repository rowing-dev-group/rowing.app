<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\News;

/* @var $this yii\web\View */
/* @var $model app\models\Competition */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="competitions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->textInput() ?>

    <?= $form->field($model, 'begin_date')->textInput() ?>

    <?= $form->field($model, 'end_date')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'water_id')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'published' => 'Published', 'unpublished' => 'Unpublished', 'delete' => 'Delete', 'arhive' => 'Arhive', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'level')->dropDownList([ 'international' => 'International', 'countries' => 'Countries', 'cities' => 'Cities', 'qualifying' => 'Qualifying', 'olympic_games' => 'Olympic games', 'continental' => 'Continental', 'world_cup' => 'World cup', 'world_championships' => 'World championships', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'news_ids')->dropDownList(News::listAll(), ['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

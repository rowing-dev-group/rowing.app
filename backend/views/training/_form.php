<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Person;

/* @var $this yii\web\View */
/* @var $model app\models\Training */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="training-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'health')->dropDownList([ 'good' => 'Good', 'normal' => 'Normal', 'bad' => 'Bad', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'training_type')->dropDownList([ 'command' => 'Command', 'personal' => 'Personal', 'command-personal' => 'Command-personal', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'persons_ids')->dropDownList(Person::listAll(), ['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Person;

/* @var $this yii\web\View */
/* @var $model app\models\PersonType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persons-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'persons_ids')->dropDownList(Person::listAll(), ['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

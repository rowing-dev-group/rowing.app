<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonType */

$this->title = 'Create Persons Type';
$this->params['breadcrumbs'][] = ['label' => 'Persons Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persons-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

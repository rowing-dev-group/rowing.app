<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persons-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'persons_id') ?>

    <?= $form->field($model, 'coach_id') ?>

    <?= $form->field($model, 'about') ?>

    <?= $form->field($model, 'city_id') ?>

    <?= $form->field($model, 'begin_carier') ?>

    <?php // echo $form->field($model, 'end_carier') ?>

    <?php // echo $form->field($model, 'social_accounts') ?>

    <?php // echo $form->field($model, 'body_type') ?>

    <?php // echo $form->field($model, 'personal_life') ?>

    <?php // echo $form->field($model, 'place_birthday') ?>

    <?php // echo $form->field($model, 'phones') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

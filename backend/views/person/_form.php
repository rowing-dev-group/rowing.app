<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\Models\Base;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persons-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'coach_id')->textInput() ?>

    <?= $form->field($model, 'about')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->textInput() ?>

    <?= $form->field($model, 'begin_carier')->textInput() ?>

    <?= $form->field($model, 'end_carier')->textInput() ?>

    <?= $form->field($model, 'social_accounts')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body_type')->dropDownList([ 'ectomorph' => 'Ectomorph', 'endomorph' => 'Endomorph', 'mesomorph' => 'Mesomorph', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'personal_life')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'place_birthday')->textInput() ?>

    <?= $form->field($model, 'phones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', 'reabilitation' => 'Reabilitation', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'bases_ids')->dropDownList(Base::listAll(), ['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

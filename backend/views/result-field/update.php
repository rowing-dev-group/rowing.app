<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ResultField */

$this->title = 'Update Results Fields: ' . $model->results_fields_id;
$this->params['breadcrumbs'][] = ['label' => 'Results Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->results_fields_id, 'url' => ['view', 'id' => $model->results_fields_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="results-fields-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ResultField */

$this->title = $model->results_fields_id;
$this->params['breadcrumbs'][] = ['label' => 'Results Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="results-fields-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->results_fields_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->results_fields_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'results_fields_id',
            'value',
            'description',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'fields_type_id',
            'training_results_id',
        ],
    ]) ?>

</div>

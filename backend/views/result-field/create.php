<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ResultField */

$this->title = 'Create Results Fields';
$this->params['breadcrumbs'][] = ['label' => 'Results Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="results-fields-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Paddle */

$this->title = $model->paddles_id;
$this->params['breadcrumbs'][] = ['label' => 'Paddles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paddles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->paddles_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->paddles_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'paddles_id',
            'paddle',
            'create_date',
            'weigth',
            'length',
            'state',
            'type_paddle',
            'details',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            'base_id',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Crew;
/* @var $this yii\web\View */
/* @var $model app\models\Paddle */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paddles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'paddle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'weigth')->textInput() ?>

    <?= $form->field($model, 'length')->textInput() ?>

    <?= $form->field($model, 'state')->dropDownList([ 'new' => 'New', 'normal' => 'Normal', 'damage' => 'Damage', 'critical' => 'Critical', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'type_paddle')->dropDownList([ 'scull' => 'Scull', 'casement_paddle' => 'Casement paddle', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'base_id')->textInput() ?>

    <?= $form->field($model, 'crews_ids')->dropDownList(Crew::listAll(), ['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

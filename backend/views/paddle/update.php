<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Paddle */

$this->title = 'Update Paddles: ' . $model->paddles_id;
$this->params['breadcrumbs'][] = ['label' => 'Paddles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->paddles_id, 'url' => ['view', 'id' => $model->paddles_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="paddles-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

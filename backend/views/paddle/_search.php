<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaddleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="paddles-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'paddles_id') ?>

    <?= $form->field($model, 'paddle') ?>

    <?= $form->field($model, 'create_date') ?>

    <?= $form->field($model, 'weigth') ?>

    <?= $form->field($model, 'length') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'type_paddle') ?>

    <?php // echo $form->field($model, 'details') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'base_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Paddle */

$this->title = 'Create Paddles';
$this->params['breadcrumbs'][] = ['label' => 'Paddles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="paddles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

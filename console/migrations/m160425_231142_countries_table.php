<?php

use yii\db\Migration;

class m160425_231142_countries_table extends Migration
{
    public function up()
    {
    $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Countries}}', [  
            'country_id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),            
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer(),
            'description' => $this->string(4096)            
        ], $tableOptions);  
    }

    public function down()
    {
     $this->dropTable('{{%Countries}}');
    }
}

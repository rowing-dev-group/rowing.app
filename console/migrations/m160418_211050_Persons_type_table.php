<?php

use yii\db\Migration;

class m160418_211050_Persons_type_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%Persons_type}}', [
            'persons_type_id' =>$this->primaryKey(),
            'name' =>$this->string(1024)->notNull(),
            'description' =>$this->string(4096),            
            'created_at' =>$this->dateTime()->notNull(),
            'created_by' =>$this->integer()->notNull(),
            'updated_at' =>$this->dateTime(),            
            'updated_by' =>$this->integer(),            
        ],$tableOptions);              
     }

    public function safeDown()
    {
        $this->dropTable('{{%Persons_type}}');
    }
}

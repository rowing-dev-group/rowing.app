<?php

use yii\db\Schema;
use yii\db\Migration;

class m160826_202828_Settings_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Settings}}', [  
            'key' => $this->string(255)->notNull(),
            'value' => $this->text()->notNull(),
            'autoload' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'type' => "enum('string', 'integer','bool', 'array', 'float') NOT NULL"
        ], $tableOptions); 
        $this->addPrimaryKey('key','Settings','key');

    }

    public function down()
    {
        $this->dropTable('{{%Settings}}');
    }
}
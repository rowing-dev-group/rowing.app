<?php

use yii\db\Migration;

class m160420_222639_News_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%News}}', [  
            'news_id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'short_text' => $this->string(2048)->notNull(),
            'text' => $this->text()->notNull(),
            'status' => "enum('draft','published', 'arhived',".
                             "'deleted', 'unpublished')NOT NULL DEFAULT 'draft'",
            'alias' => $this->string(100)->unique(),
            'meta_description' => $this->string(4096)->notNull(),            
            'meta_keywords' => $this->string(4096)->notNull(),                        
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(),        
            'rubrics_id' => $this->integer()->notNull()
        ], $tableOptions);      
    }

    public function down()
    {
        $this->dropTable('{{%News}}');
    }   
}
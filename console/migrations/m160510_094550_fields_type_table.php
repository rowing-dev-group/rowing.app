<?php

use yii\db\Migration;

class m160510_094550_fields_type_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';            
        }

        $this->createTable('{{%Fields_type}}',[
            'fields_type_id' => $this->primaryKey(),
            'name' => $this->string(2048)->notNull(),
            'details' => $this->string(4096),
            'type' => "enum( 'DOUBLE', 'TIME')",
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'results_type_id' => $this->integer()->notNull(),
        ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Fields_type}}');
    }
}

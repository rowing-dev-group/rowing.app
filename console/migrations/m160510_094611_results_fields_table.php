<?php
use yii\db\Schema;
use yii\db\Migration;

class m160510_094611_results_fields_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';            
        }

        $this->createTable('{{%Results_fields}}',[
            'results_fields_id' => $this->primaryKey(),
            'value' =>Schema::TYPE_DOUBLE . ' NOT NULL',
            'description' => $this->string(4096),
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'fields_type_id' => $this->integer()->notNull(),
            'training_results_id' => $this->integer()->notNull(),
        ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Results_fields}}');
    }
}

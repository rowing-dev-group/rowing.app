<?php

use yii\db\Migration;

class m160628_111726_Crews_paddles_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';                   
        }

        $this->createTable('{{%Crews_paddles}}', [
            'crews_id' => $this->integer()->notNull(),
            'paddles_id' => $this->integer()->notNull(),
            'PRIMARY KEY(crews_id, paddles_id)'
            ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Crews_paddles}}');
    }
}

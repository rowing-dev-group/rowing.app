<?php

use yii\db\Migration;

class m160420_222739_Rubrics_table extends Migration
{
    public function up()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Rubrics}}', [  
            'rubrics_id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
            'parent_id' => $this->integer(),
            'level' => $this->integer()->notNull(),            
            'status' => "enum('active', 'inactive', 'deleted')NOT NULL DEFAULT 'active'",
            'description'=> $this->string(4096),
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' =>$this->integer()
        ], $tableOptions);      
    }

    public function down()
    {
        $this->dropTable('{{%Rubrics}}');
    }
}

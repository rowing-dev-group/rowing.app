<?php

use yii\db\Migration;

class m160427_215228_Competition_table extends Migration
{
    public function up()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Competitions}}', [  
            'competition_id' => $this->primaryKey(),
            'name' => $this->string(2048)->notNull(),
            'city_id' => $this->integer()->notNull(),
            'begin_date' => $this->dateTime()->notNull(),
            'end_date' => $this->dateTime(),
            'description' => $this->text(),            
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer(),
            'water_id' => $this->integer()->notNull(),
            'status' =>"enum('published','unpublished','delete',".
                       "'arhive') NOT NULL DEFAULT 'unpublished'", 
            'level' =>"enum('international','countries','cities',".
                      "'qualifying','olympic_games','continental',".
                      "'world_cup', 'world_championships') NOT NULL".
                      " DEFAULT 'countries'"                                                 
        ], $tableOptions);   
    }

    public function down()
    {
        $this->dropTable('{{%Competitions}}');
    }

}

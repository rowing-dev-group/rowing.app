<?php

use yii\db\Migration;

class m160628_113105_Competitions_news_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';                   
        }

        $this->createTable('{{%Competitions_news}}', [
            'competition_id' => $this->integer()->notNull(),
            'news_id' => $this->integer()->notNull(),
            'PRIMARY KEY(competition_id, news_id)'
            ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Competitions_news}}');
    }
}

<?php

use yii\db\Migration;

class m160628_110906_Crews_persons_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';                   
        }

        $this->createTable('{{%Crews_persons}}', [
            'crews_id' => $this->integer()->notNull(),
            'persons_id' => $this->integer()->notNull(),
            'PRIMARY KEY(crews_id, persons_id)'
            ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Crews_persons}}');
    }
}

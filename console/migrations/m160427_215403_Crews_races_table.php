<?php

use yii\db\Migration;

class m160427_215403_Crews_races_table extends Migration
{
    public function up()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Crews_races}}', [  
            'crews_id' => $this->integer()->notNull(),
            'race_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(), 
            'line_number' => $this->integer()->notNull(),
            'time_2000' => $this->integer()->notNull(),
            'time_500' => $this->integer(),
            'time_1000' => $this->integer(),
            'time_1500' => $this->integer(),
            'temp_500' => $this->integer(),
            'temp_1000' => $this->integer(),
            'temp_1500' => $this->integer(),
            'temp_2000' => $this->integer(),
            'temp_avg' => $this->integer(),
            'position_500' => $this->integer(),
            'position_1000' => $this->integer(),
            'position_1500' => $this->integer(),
            'position_2000' => $this->integer()->notNull(),
            'PRIMARY KEY(crews_id, race_id)'
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Crews_races}}');
    }
}

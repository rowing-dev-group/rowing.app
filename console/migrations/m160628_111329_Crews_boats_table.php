<?php

use yii\db\Migration;

class m160628_111329_Crews_boats_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';                   
        }

        $this->createTable('{{%Crews_boats}}', [
            'crews_id' => $this->integer()->notNull(),
            'boats_id' => $this->integer()->notNull(),
            'PRIMARY KEY(crews_id, boats_id)'
            ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Crews_boats}}');
    }
}

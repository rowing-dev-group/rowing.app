<?php

use yii\db\Migration;

class m160510_094533_training_results_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Training_results}}',[
            'training_results_id' => $this->primaryKey(),
            'results_type_id' => $this->integer()->notNull(),
            'training_id' => $this->integer()->notNull(),
        ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Training_results}}');
    }
}

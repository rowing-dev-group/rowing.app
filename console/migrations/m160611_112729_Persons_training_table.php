<?php

use yii\db\Migration;

class m160611_112729_Persons_training_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';                   
        }

        $this->createTable('{{%Persons_training}}', [
            'persons_id' => $this->integer()->notNull(),
            'training_id' => $this->integer()->notNull(),
            'PRIMARY KEY(persons_id, training_id)'
            ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Persons_training}}');
    }
}

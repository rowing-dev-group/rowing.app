<?php

use yii\db\Migration;

class m160427_215308_Races_table extends Migration
{
    public function up()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Races}}', [  
            'race_id' => $this->primaryKey(),
            'description' => $this->text(),
            'distance' => $this->integer()->notNull(),
            'start_date' => $this->dateTime(),
            'heat' => $this->integer()->notNull(),
            'progression' => $this->string(1024)->notNull(),
            'weather' => "enum('favourable','headwind','crosswind',".
                              "'calm','storm')NOT NULL DEFAULT 'calm'",
            'status' => "enum('published','unpublished','cancelled',".
                        "'waiting','finished','deleted','arhive')NOT".
                        " NULL DEFAULT 'unpublished'",
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'judge_id' => $this->integer(),
            'competition_id' => $this->integer()                       
        ], $tableOptions);   
    }

    public function down()
    {
        $this->dropTable('{{%Races}}');
    }
}

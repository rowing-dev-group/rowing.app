<?php

use yii\db\Migration;

class m160628_185001_Relationship_table extends Migration
{
    public function up()
      {
        // ***Block_persons***

        // Users_table
        $this->addForeignKey('fk-Users-Persons-persons_id', '{{%Users}}', 'persons_id', '{{%Persons}}', 'persons_id', 'NO ACTION');  

        // Persons_table
        $this->addForeignKey('fk-Persons-City-city_id', '{{%Persons}}', 'city_id', '{{%City}}', 'city_id', 'CASCADE');
        $this->addForeignKey('fk-Persons-City-place_birthday', '{{%Persons}}', 'place_birthday', '{{%City}}', 'city_id', 'CASCADE');

        // Persons_type_table
        $this->addForeignKey('fk-Persons_type-Users-created_by', '{{%Persons_type}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Persons_type-Users-updated_by', '{{%Persons_type}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');

        // Persons_type_persons_table
        $this->addForeignKey('fk-Persons_type_persons-Persons_type-persons_type_id', '{{%Persons_type_persons}}', 'persons_type_id', '{{%Persons_type}}', 'persons_type_id', 'CASCADE');
        $this->addForeignKey('fk-Persons_type_persons-Persons-persons_id', '{{%Persons_type_persons}}', 'persons_id', '{{%Persons}}', 'persons_id', 'CASCADE');

        // Crews_table
        $this->addForeignKey('fk-Crews-Users-created_at', '{{%Crews}}', 'created_by', '{{%Users}}', 'user_id', 'CASCADE');
        $this->addForeignKey('fk-Crews-Users-updated_by', '{{%Crews}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');        

        // Crews_persons_table
        $this->addForeignKey('fk-Crews_persons-Crews-crews_id', '{{%Crews_persons}}', 'crews_id', '{{%Crews}}', 'crews_id', 'CASCADE');                
        $this->addForeignKey('fk-Crews_persons-Persons-persons_id', '{{%Crews_persons}}', 'persons_id', '{{%Persons}}', 'persons_id', 'CASCADE');                

        // Boats_table
        $this->addForeignKey('fk-Boats-Users-created_by', '{{%Boats}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Boats-Users-updated_by', '{{%Boats}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Boats-Bases-base_id', '{{%Boats}}', 'base_id', '{{%Bases}}', 'base_id', 'CASCADE');

        // Paddles_table
        $this->addForeignKey('fk-Paddles-Users-created_by', '{{%Paddles}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Paddles-Users-updated_by', '{{%Paddles}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Paddles-Bases-base_id', '{{%Paddles}}', 'base_id', '{{%Bases}}', 'base_id', 'CASCADE');

        // Crews_boats_table
        $this->addForeignKey('fk-Crews-Boats-crews-crews_id', '{{%Crews_boats}}', 'crews_id', '{{%Crews}}', 'crews_id', 'CASCADE');
        $this->addForeignKey('fk-Crews-Boats-boats-boats_id', '{{%Crews_boats}}', 'boats_id', '{{%Boats}}', 'boats_id', 'CASCADE');
        
        //Crews_paddles_table
        $this->addForeignKey('fk-Crews_paddles-Crews-crews_id', '{{%Crews_paddles}}', 'crews_id', '{{%Crews}}', 'crews_id', 'CASCADE');
        $this->addForeignKey('fk-Crews-Paddles-paddles-paddles_id', '{{%Crews_paddles}}', 'paddles_id', '{{%Paddles}}', 'paddles_id', 'CASCADE');

        // ***Block_news***

        // News_table
        $this->addForeignKey('fk-News-Users-created_by', '{{%News}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-News-Users-updated_by', '{{%News}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-News-Rubrics-rubrics_id', '{{%News}}', 'rubrics_id', '{{%Rubrics}}', 'rubrics_id', 'SET NULL');

        // Comments_table
        $this->addForeignKey('fk-Comments-Users-created_by', '{{%Comments}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Comments-Users-updated_by', '{{%Comments}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Comments-News-news_id', '{{%Comments}}', 'news_id', '{{%News}}', 'news_id', 'CASCADE');        

        // Rubrics_table
        $this->addForeignKey('fk-Rubrics-Users-created_by', '{{%Rubrics}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Rubrics-Users-updated_by', '{{%Rubrics}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');

        // ***Block_place***

        // City_table
        $this->addForeignKey('fk-City-Countries-country_id', '{{%City}}', 'country_id', '{{%Countries}}', 'country_id', 'CASCADE');

        // Base_persons_table
        $this->addForeignKey('fk-Base_persons-Bases-base_id', '{{%Base_persons}}', 'base_id', '{{%Bases}}', 'base_id', 'CASCADE');        
        $this->addForeignKey('fk-Base_persons-Persons-persons_id', '{{%Base_persons}}', 'persons_id', '{{%Persons}}', 'persons_id', 'CASCADE');        

        // ***Block_competitions***

         // Competition_table
        $this->addForeignKey('fk-Competitions-Waters-water_id', '{{%Competitions}}', 'water_id', '{{%Waters}}', 'water_id', 'NO ACTION');
        $this->addForeignKey('fk-Competitions-Users-created_by', '{{%Competitions}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Competitions-Users-updated_by', '{{%Competitions}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Competitions-City-city_id', '{{%Competitions}}', 'city_id', '{{%City}}', 'city_id', 'SET NULL');


        // Competitions_news_table
        $this->addForeignKey('fk-Competitions_news-Competitions-competitions_id', '{{%Competitions_news}}', 'competition_id', '{{%Competitions}}', 'competition_id', 'CASCADE');        
        $this->addForeignKey('fk-Competitions_news-News-news_id', '{{%Competitions_news}}', 'news_id', '{{%News}}', 'news_id', 'CASCADE');        


        // Waters_table
        $this->addForeignKey('fk-Waters-City-city_id', '{{%Waters}}', 'city_id', '{{%City}}', 'city_id', 'SET NULL');
        $this->addForeignKey('fk-Waters-Users-created_by', '{{%Waters}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Waters-Users-updated_by', '{{%Waters}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');

        // Races_table
        $this->addForeignKey('fk-Races-Competitions-competitions_id', '{{%Races}}', 'competition_id', '{{%Competitions}}', 'competition_id', 'CASCADE');
        $this->addForeignKey('fk-Waters-Persons-persons_id', '{{%Races}}', 'judge_id', '{{%Persons}}', 'persons_id', 'SET NULL');
        $this->addForeignKey('fk-Races-Users-created_by', '{{%Races}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Races-Users-updated_by', '{{%Races}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');

        // Crews_races_table
        $this->addForeignKey('fk-Crews_races-Crews-crews_id', '{{%Crews_races}}', 'crews_id', '{{%Crews}}', 'crews_id', 'CASCADE');    
        $this->addForeignKey('fk-Crews_races-Races-race_id', '{{%Crews_races}}', 'race_id', '{{%Races}}', 'race_id', 'CASCADE');    
        $this->addForeignKey('fk-Crews_races-Users-created_by', '{{%Crews_races}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Crews_races-Users-updated_by', '{{%Crews_races}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');

        // ***Block_results***        

        // Persons_training_table
        $this->addForeignKey('fk-Persons_training-Training-training_id','{{%Persons_training}}','training_id', '{{%Training}}', 'training_id', 'CASCADE');
        $this->addForeignKey('fk-Persons_training-Persons-persons_id','{{%Persons_training}}','persons_id', '{{%Persons}}', 'persons_id', 'CASCADE');
         
        //Training_table
        $this->addForeignKey('fk-Training-Users-created_by', '{{%Training}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Training-Users-updated_by', '{{%Training}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');

        // Training_results_table
        $this->addForeignKey('fk-Training_results-Training-training_id','{{%Training_results}}', 'training_id', '{{%Training}}', 'training_id', 'CASCADE');
        $this->addForeignKey('fk-Training_results-Results_type-results_type_id','{{%Training_results}}', 'results_type_id', '{{%Results_type}}','results_type_id', 'NO ACTION');

        // Results_type_table 
        $this->addForeignKey('fk-Results_type-Users-created_by', '{{%Results_type}}', 'created_by', '{{%Users}}', 'user_id','NO ACTION');
        $this->addForeignKey('fk-Results_type-Users-updated_by', '{{%Results_type}}', 'updated_by', '{{%Users}}', 'user_id','NO ACTION');
    
        // Fields_type_table
        $this->addForeignKey('fk-Fields_type-Results_type-results_type_id', '{{%Fields_type}}', 'results_type_id', '{{%Results_type}}', 'results_type_id','CASCADE');
        $this->addForeignKey('fk-Fields_type-Users-created_by', '{{%Fields_type}}', 'created_by', '{{%Users}}', 'user_id','NO ACTION');
        $this->addForeignKey('fk-Fields_type-Users-updated_by', '{{%Fields_type}}', 'updated_by', '{{%Users}}', 'user_id','NO ACTION');

        // Results_fields_table
        $this->addForeignKey('fk-Results_fields-Fields_type-fields_type_id', '{{%Results_fields}}', 'fields_type_id', '{{%Fields_type}}', 'fields_type_id', 'CASCADE');
        $this->addForeignKey('fk-Results_fields-Training_results-training_results_id', '{{%Results_fields}}', 'training_results_id', '{{%Training_results}}', 'training_results_id', 'CASCADE');
        $this->addForeignKey('fk-Results_fields-Users-created_by', '{{%Results_fields}}', 'created_by', '{{%Users}}', 'user_id','NO ACTION');
        $this->addForeignKey('fk-Results_fields-Users-updated_by', '{{%Results_fields}}', 'updated_by', '{{%Users}}', 'user_id','NO ACTION');
        
        // Bases
        $this->addForeignKey('fk-Bases-Users-created-by','{{%Bases}}', 'created_by', '{{%Users}}', 'user_id', 'NO ACTION');
        $this->addForeignKey('fk-Bases-Users-updated-by','{{%Bases}}', 'updated_by', '{{%Users}}', 'user_id', 'NO ACTION');
    } 

    public function down()
    {
        // ***Block_persons***

        // Users_table
        $this->dropForeignKey('fk-Users-Persons-persons_id', '{{%Users}}');

        // Persons_table
        $this->dropForeignKey('fk-Persons-City-city_id', '{{%Persons}}');
        $this->dropForeignKey('fk-Persons-City-place_birthday', '{{%Persons}}');

        // Persons_type_table
        $this->dropForeignKey('fk-Persons_type-Users-created_by', '{{%Persons_type}}');
        $this->dropForeignKey('fk-Persons_type-Users-updated_by', '{{%Persons_type}}');

        // Persons_type_persons_table
        $this->dropForeignKey('fk-Persons_type_persons-Persons_type-persons_type_id', '{{%Persons_type_persons}}');
        $this->dropForeignKey('fk-Persons_type_persons-Persons-persons_id', '{{%Persons_type_persons}}');

        // Crews_table
        $this->dropForeignKey('fk-Crews-Users-created_at', '{{%Crews}}');
        $this->dropForeignKey('fk-Crews-Users-updated_by', '{{%Crews}}');        

        // Crews_persons_table
        $this->dropForeignKey('fk-Crews_persons-Crews-crews_id', '{{%Crews_persons}}');                
        $this->dropForeignKey('fk-Crews_persons-Persons-persons_id', '{{%Crews_persons}}');                

        // Boats_table
        $this->dropForeignKey('fk-Boats-Users-created_by', '{{%Boats}}');
        $this->dropForeignKey('fk-Boats-Users-updated_by', '{{%Boats}}');
        $this->dropForeignKey('fk-Boats-Bases-base_id', '{{%Boats}}');

        // Paddles_table
        $this->dropForeignKey('fk-Paddles-Users-created_by', '{{%Paddles}}');
        $this->dropForeignKey('fk-Paddles-Users-updated_by', '{{%Paddles}}');
        $this->dropForeignKey('fk-Paddles-Bases-base_id', '{{%Paddles}}');

        // Crews_boats_table
        $this->dropForeignKey('fk-Crews-Boats-crews-crews_id', '{{%Crews_boats}}');
        $this->dropForeignKey('fk-Crews-Boats-boats-boats_id', '{{%Crews_boats}}');
        
        //Crews_paddles_table
        $this->dropForeignKey('fk-Crews_paddles-Crews-crews_id', '{{%Crews_paddles}}');
        $this->dropForeignKey('fk-Crews-Paddles-paddles-paddles_id', '{{%Crews_paddles}}');

        // ***Block_news***

        // News_table
        $this->dropForeignKey('fk-News-Users-created_by', '{{%News}}');
        $this->dropForeignKey('fk-News-Users-updated_by', '{{%News}}');
        $this->dropForeignKey('fk-News-Rubrics-rubrics_id', '{{%News}}');

        // Comments_table
        $this->dropForeignKey('fk-Comments-News-news_id', '{{%Comments}}');        
        $this->dropForeignKey('fk-Comments-Users-created_by', '{{%Comments}}');
        $this->dropForeignKey('fk-Comments-Users-updated_by', '{{%Comments}}');

        // Rubrics_table
        $this->dropForeignKey('fk-Rubrics-Users-created_by','{{%Rubrics}}');
        $this->dropForeignKey('fk-Rubrics-Users-updated_by','{{%Rubrics}}');

        // ***Block_place***

        // City_table
        $this->dropForeignKey('fk-City-Countries-country_id', '{{%City}}');

        // Base_persons_table
        $this->dropForeignKey('fk-Base_persons-Bases-base_id', '{{%Base_persons}}');        
        $this->dropForeignKey('fk-Base_persons-Persons-persons_id', '{{%Base_persons}}');        

        // ***Block_competitions***

        // Competition_table
        $this->dropForeignKey('fk-Competitions-Waters-water_id', '{{%Competitions}}');
        $this->dropForeignKey('fk-Competitions-Users-created_by', '{{%Competitions}}');
        $this->dropForeignKey('fk-Competitions-Users-updated_by', '{{%Competitions}}');
        $this->dropForeignKey('fk-Competitions-City-city_id', '{{%Competitions}}');

        // Competitions_news_table
        $this->dropForeignKey('fk-Competitions_news-Competitions-competitions_id', '{{%Competitions_news}}');        
        $this->dropForeignKey('fk-Competitions_news-News-news_id', '{{%Competitions_news}}');        

        // Waters_table
        $this->dropForeignKey('fk-Waters-City-city_id', '{{%Waters}}');
        $this->dropForeignKey('fk-Waters-Users-created_by', '{{%Waters}}');
        $this->dropForeignKey('fk-Waters-Users-updated_by', '{{%Waters}}');

        // Races_table
        $this->dropForeignKey('fk-Races-Competitions-competitions_id', '{{%Races}}');
        $this->dropForeignKey('fk-Waters-Persons-persons_id', '{{%Races}}');
        $this->dropForeignKey('fk-Races-Users-created_by', '{{%Races}}');
        $this->dropForeignKey('fk-Races-Users-updated_by', '{{%Races}}');

        // Crews_races_table
        $this->dropForeignKey('fk-Crews_races-Crews-crews_id', '{{%Crews_races}}');    
        $this->dropForeignKey('fk-Crews_races-Races-race_id', '{{%Crews_races}}');    
        $this->dropForeignKey('fk-Crews_races-Users-created_by', '{{%Crews_races}}');
        $this->dropForeignKey('fk-Crews_races-Users-updated_by', '{{%Crews_races}}');

        // ***Block_results***        

        // Persons_training_table
        $this->dropForeignkey('fk-Persons_training-Training-training_id','{{%persons_training}}');
        $this->dropForeignkey('fk-Persons_training-Persons-persons_id','{{%persons_training}}');
         
        //Training_table
        $this->dropForeignKey('fk-Training-Users-created_by', '{{%Training}}');
        $this->dropForeignKey('fk-Training-Users-updated_by', '{{%Training}}');

        // Training_results_table
        $this->dropForeignKey('fk-Training_results-Training-training_id','{{%Training_results}}');
        $this->dropForeignKey('fk-Training_results-Results_type-results_type_id','{{%Training_results}}');

        // Results_type_table 
        $this->dropForeignKey('fk-Results_type-Users-created_by', '{{%Results_type}}');
        $this->dropForeignKey('fk-Results_type-Users-updated_by', '{{%Results_type}}');
    
        // Fields_type_table
        $this->dropForeignKey('fk-Fields_type-Results_type-results_type_id', '{{%Fields_type}}');
        $this->dropForeignKey('fk-Fields_type-Users-created_by', '{{%Fields_type}}', 'created_by');
        $this->dropForeignKey('fk-Fields_type-Users-updated_by', '{{%Fields_type}}', 'updated_by');

        // Results_fields_table
        $this->dropForeignKey('fk-Results_fields-Fields_type-fields_type_id', '{{%Results_fields}}');
        $this->dropForeignKey('fk-Results_fields-Training_results-training_results_id', '{{%Results_fields}}');
        $this->dropForeignKey('fk-Results_fields-Users-created_by', '{{%Results_fields}}');
        $this->dropForeignKey('fk-Results_fields-Users-updated_by', '{{%Results_fields}}');  

        // Bases
        $this->dropForeignKey('fk-Bases-Users-created-by','{{%Bases}}');
        $this->dropForeignKey('fk-Bases-Users-updated-by','{{%Bases}}');
    }
}
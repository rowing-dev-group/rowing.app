<?php

use yii\db\Migration;

class m160628_184001_alter_Comments extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%Comments}}','created_by', 'integer NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%Comments}}','created_by', 'integer NOT NULL');
    }
}

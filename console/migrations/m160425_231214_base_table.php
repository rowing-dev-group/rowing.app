<?php

use yii\db\Migration;

class m160425_231214_base_table extends Migration
{
    public function up()
    {
                    $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Bases}}', [  
            'base_id' => $this->primaryKey(),
            'name' => $this->string(1024)->notNull(),            
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer(),
            'address' => $this->string(4096)->notNull(),
            'city_id' => $this->integer()->notNull(),
            'water_id' => $this->integer(),            
            'description' => $this->string(4096),            
            'phones' => $this->string(3072),            
            'email' => $this->string()->notNull(),            
            'work_time' => $this->string(1024)->notNull(),                        
            'date_founding' => $this->dateTime()->notNull(),                        
            'director' => $this->integer()                        
        ], $tableOptions); 
    }

    public function down()
    {
        $this->dropTable('{{%Bases}}');
    }
}

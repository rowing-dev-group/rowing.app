<?php

use yii\db\Schema;
use yii\db\Migration;

class m160427_215253_Waters_table extends Migration
{
    public function up()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Waters}}', [  
            'water_id' => $this->primaryKey(),
            'name' => $this->string(512)->notNull(),
            'lat' => Schema::TYPE_FLOAT . ' NOT NULL',
            'lng' => Schema::TYPE_FLOAT . ' NOT NULL',
            'city_id' => $this->integer()->notNull(),
            'founding_date' => $this->dateTime(),           
            'description' => $this->text(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'created_by' => $this->integer()->notNull(),
        ], $tableOptions);   
    }

    public function down()
    {
        $this->dropTable('{{%Waters}}'); 
    }
}    

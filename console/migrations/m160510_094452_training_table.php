<?php

use yii\db\Migration;

class m160510_094452_training_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';            
        }

        $this->createTable('{{%Training}}',[
            'training_id' => $this->primaryKey(),
            'health' => "enum('good', 'normal', 'bad') NOT NULL DEFAULT 'normal'",
            'training_type' => "enum('command', 'personal', 'command-personal') NOT NULL DEFAULT 'personal'",
            'code' => $this->string(32)->notNull(),
            'date' => $this->dateTime()->notNull(),
            'description' => $this->string(4096),
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
        ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Training}}');
    }
}

<?php

use yii\db\Migration;

class m160628_110049_Persons_type_persons_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';                   
        }

        $this->createTable('{{%Persons_type_persons}}', [
            'persons_type_id' => $this->integer()->notNull(),
            'persons_id' => $this->integer()->notNull(),
            'PRIMARY KEY(persons_type_id, persons_id)'
            ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%Persons_type_persons}}');
    }
}

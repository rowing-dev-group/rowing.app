<?php

use yii\db\Migration;

class m160423_151245_Boats_table extends Migration
{
    public function up()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Boats}}', [  
            'boats_id' => $this->primaryKey(),
            'manufacturer' => $this->string(255)->notNull(),
            'category'=> "enum('LM1x', 'LM2-', 'LM2x', 'LM4-',".
                              "'M2+','LM4x','LM8+', 'M1x',".
                              "'M2-', 'M2x', 'M4-', 'M4+',".
                              "'M4x', 'M8+', 'LW1x', 'LW2x',".
                              "'LW4x','W1x', 'W2-', 'W2x',".
                              "'W4-','W4x', 'W8+', 'ASM1x',".
                              "'ASW1x','LTAMix4+')NOT NULL DEFAULT 'M1x'",
            'create_date' => $this->date(),
            'weigth' => $this->integer(),
            'length' => $this->integer(),
            'state' => "enum('new','normal','damage',".
                            "'critical')NOT NULL DEFAULT 'normal'",            
            'bow_type' =>"enum('acute','straigth')",
            'details' =>$this->string(4096),            
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer(),
            'reiggers' => "enum('carbon','aluminium')",
            'base_id' =>  $this->integer()           
        ], $tableOptions);  
    }

    public function down()
    {
        $this->dropTable('{{%Boats}}');
    }
}

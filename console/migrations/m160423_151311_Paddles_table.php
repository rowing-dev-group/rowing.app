<?php

use yii\db\Migration;

class m160423_151311_Paddles_table extends Migration
{
    public function up()
    {
                 $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Paddles}}', [  
            'paddles_id' => $this->primaryKey(),
            'paddle' => $this->string()->notNull(),
            'create_date' => $this->date(),
            'weigth' => $this->integer(),
            'length' => $this->integer(),
            'state' => "enum('new','normal','damage','critical')NOT NULL DEFAULT 'normal'",            
            'type_paddle' =>"enum('scull','casement_paddle')NOT NULL DEFAULT 'scull'",
            'details' => $this->string(4096),            
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer(),
            'base_id' =>  $this->integer() 
        ], $tableOptions);  
    }

    public function down()
    {
        $this->dropTable('{{%Paddles}}');
    }
}

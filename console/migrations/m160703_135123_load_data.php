<?php

use yii\db\Migration;
use yii\db\Query;

class m160703_135123_load_data extends Migration
{
    private $user_ids = [];
    private $country_ids = [];
    private $city_ids = [];
    private $persons_ids = [];
    private $rubrics_ids = [];
    private $news_ids = [];
    private $comments_ids = [];
    private $water_ids = [];
    private $base_ids = [];
    private $competition_ids = [];
    private $crews_ids = [];
    private $race_ids = [];
    private $boats_ids = [];
    private $paddles_ids = [];
    private $persons_type_ids = [];
    private $results_type_ids = [];
    private $training_ids = [];
    private $fields_type_ids = [];
    private $training_results_ids = [];

    public function safeUp()
    {
        $faker = Faker\Factory::create();
        $this->load_users($faker);
        $this->load_country($faker);
        $this->load_city($faker);
        $this->load_persons($faker);
        $this->load_rubrics($faker);
        $this->load_news($faker);
        $this->load_comments($faker);
        $this->load_waters($faker);
        $this->load_competitions($faker);
        $this->load_bases($faker);
        $this->load_paddles($faker);
        $this->load_boats($faker);
        $this->load_crews($faker);
        $this->load_races($faker);
        $this->load_crews_races();
        $this->load_competitions_news();
        $this->load_crews_persons();
        $this->load_base_persons();
        $this->load_crews_boats();
        $this->load_crews_paddles();
        $this->load_persons_type($faker);
        $this->load_persons_type_persons();
        $this->load_training($faker);
        $this->load_results_type($faker);
        $this->load_fields_type($faker);
        $this->load_training_results();
        $this->load_results_fields($faker);
        $this->load_persons_training();
    }

    private function rand_null ($f)
    {
        $rand_or_null = [$f, null];
        $rand = array_rand($rand_or_null);
        return $rand_or_null[$rand];
    }

    private function rand_ids($ids)
    {
       $rand = array_rand($ids);
       return $ids[$rand];
    }  

    private function City_from_DB()
    {
        $City = new Query();
        $City->select('city_id')->from('City');
        if ($City->all() !== null) {
            $city_id = $this->rand_ids($City->all());
            return $city_id['city_id'];
        }
    }

    private function Rubrics_from_DB()
    {
        $Rubrics = new Query();
        $Rubrics->select('rubrics_id')->from('Rubrics');
        if ($Rubrics->all() !== null) {
           $rubrics_id = $this->rand_ids($Rubrics->all());
           return $rubrics_id['rubrics_id'];
        }   
    }

    public function load_users($faker)
    {
        $users = [];
        $password_hash = Yii::$app->getSecurity()->generatePasswordHash('demo123');
        $status = ['active','inactive','deleted'];
        $prefix = ['Ms_','Mr_', 'Jr_', 'Dr_', 'G_', 'A_',
                       'B_', 'S_', 'W_', 'Q_', 'AS_', 'R_',
                       'Ro_','Rower_', 'ASq_', 'Pre_','Gos_', 'Lap_',
                       'Proger_', 'Max_', 'Z_Z_', 'XX_','AA_','BB_',
                       'ASS_', 'SD_','ASD_','LS_','LW_','WW_','rowing',
                       'regata','under_23','junior','continental','bla',
                       'fora','bla_bla','manda','whila','ifa','empta'];

        $exists = new Query();
        $exists->select('user_id')->from('Users')->limit(1);

        if ($exists->all() == null) { 
            // admin
            $this->insert('Users', [
                'username' => 'admin',
                'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
                'password_hash' => $password_hash,
                'email' => $faker->email,
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'firstname' => $faker->firstname,
                'lastname' => $faker->lastname,
                'middlename' => $faker->firstname,
                'birthday' => $this->rand_null(date($format = 'Y-m-d')),
                'status' => 'active'
            ]);

            // moder
            $this->insert('Users', [
                'username' => 'moder',
                'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
                'password_hash' => $password_hash,
                'email' => $faker->email,
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'firstname' => $faker->firstname,
                'lastname' => $faker->lastname,
                'middlename' => $faker->firstname,
                'birthday' => date($format = 'Y-m-d'),
                'status' => 'active'
             ]);

            // redactor
            $this->insert('Users', [
                'username' => 'redactor',
                'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
                'password_hash' => $password_hash,
                'email' => $faker->email,
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'firstname' => $faker->firstname,
                'lastname' => $faker->lastname,
                'middlename' => $faker->firstname,
                'birthday' => date($format = 'Y-m-d'),
                'status' => 'active'
            ]);       
        }
        
        for ($i=1; $i < 150; $i++) 
        {   


            $pref = $this->rand_ids($prefix);
            $username = "$pref".$faker->firstName;
            if (!in_array($username, $users)) {
                $this->insert('Users', [
                    'username' => $username,
                    'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
                    'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('password_'. $index),
                    'email' => $faker->email,
                    'created_at' => date($format = 'Y-m-d'),
                    'updated_at' => $this->rand_null( date($format = 'Y-m-d')),
                    'firstname' => $faker->firstname,
                    'lastname' => $faker->lastname,
                    'middlename' => $this->rand_null($faker->firstname),
                    'birthday' => $this->rand_null( date($format = 'Y-m-d')),
                    'status' => array_rand($status)
                ]);
                array_push($this->user_ids, Yii::$app->db->getLastInsertID());
                array_push($users, $username);
            }
        }
    }

    public function load_country($faker)
    {
        $chek_countries = [];
        $Countries = new Query();
        $Countries->select('name')->from('Countries')->limit(150);
        if ($Countries->all() == null) {
            for ($i=1; $i < 190; $i++)
            {
                $country = $faker->country;
                if (!in_array($country, $chek_countries)) {
                    $this->insert('Countries', [
                        'name' => $country,
                        'created_at' => date($format = 'Y-m-d'),
                        'created_by' => $this->rand_ids($this->user_ids),
                        'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                        'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                        'description' => $this->rand_null($faker->text)
                    ]);
                    array_push($this->country_ids, Yii::$app->db->getLastInsertID());
                    array_push($chek_countries, $country);
                }
            }
        }
    }

    public function load_city($faker)
    {
        $check_city = [];
        $City = new Query();
        $City->select('name')->from('City')->limit(150);
        if ($City->all() == null) {
            for ($i=1; $i < 800; $i++) {
                $city = $faker->city;
                if (!in_array($city, $check_city)) {
                    $this->insert('City', [
                        'name' => $city,
                        'created_at' => date($format = 'Y-m-d'),
                        'created_by' => $this->rand_ids($this->user_ids),
                        'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                        'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                        'description' => $this->rand_null($faker->text),
                        'country_id' => $this->rand_ids($this->country_ids)
                    ]);
                    array_push($this->city_ids, Yii::$app->db->getLastInsertID());
                    array_push($check_city, $city);
                }
            }
        }        
    }

    public function load_persons($faker)
    {   
        $status = ['active', 'inactive', 'reabilitation'];
        $body = ['ectomorph', 'endomorph', 'mesomorph'];
        for ($i=1; $i < 120; $i++) {
            if (empty($this->city_ids)){
                $city_id = $this->City_from_DB();
            }else{
                $city_id = $this->rand_ids($this->city_ids);
            }
            $this->insert('Persons', [
                'coach_id' => $this->rand_null( $this->rand_ids($this->user_ids)),
                'about' => $this->rand_null($faker->text),
                'city_id' => $city_id,
                'begin_carier' => date($format = 'Y-m-d'),
                'end_carier' => $this->rand_null(date($format = 'Y-m-d')),
                'social_accounts' => $faker->userName,
                'body_type' => $this->rand_ids($body),
                'personal_life' => $this->rand_null($format->text),
                'place_birthday' => $this->rand_ids($this->city_ids),
                'phones' => $this->rand_null($faker->phoneNumber),
                'status' => $this->rand_ids($status)
            ]);
            array_push($this->persons_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_rubrics($faker)
    {
        $pair = [];
        $check_arr = [];
        $status = ['active', 'inactive', 'deleted'];
        $prefix = ['Ms_','Mr_', 'Jr_', 'Dr_', 'G_', 'A_',
                       'B_', 'S_', 'W_', 'Q_', 'AS_', 'R_',
                       'Ro_','Rower_', 'ASq_', 'Pre_','Gos_', 'Lap_',
                       'Proger_', 'Max_', 'Z_Z_', 'XX_','AA_','BB_',
                       'ASS_', 'SD_','ASD_','LS_','LW_','WW_','rowing',
                       'regata','under_23','junior','continental','bla',
                       'fora','bla_bla','manda','whila','ifa','empta'];

        for ($i=1; $i < 400; $i++) {
            $pref = $this->rand_ids($prefix);
            $rubrics = "$pref".$faker->firstname;
            if (empty($pair)) {
                $parent_id = null;
                $level = 0;                
            }else
            {
                if ($this->rand_null($this->rand_ids($pair)) == null){
                    $parent_id = null;
                    $level = 0;
                }else
                {
                    $parent_id_level = $this->rand_ids($pair);
                    $parent_id = $parent_id_level[0];
                    $level = $parent_id_level[1] + 1;
                }                
            }
            if (!in_array($rubrics, $check_arr)) {
                $this->insert('Rubrics', [
                    'name' => $rubrics,                       
                    'parent_id' => $parent_id,
                    'level' => $level,
                    'status' => array_rand($status),
                    'description' => $this->rand_null( $faker->text),
                    'created_at' => date($format = 'Y-m-d'),
                    'created_by' => $this->rand_ids($this->user_ids),
                    'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                    'updated_by' => $this->rand_null($this->rand_ids($this->user_ids))
                ]);
                array_push($this->rubrics_ids, Yii::$app->db->getLastInsertID());
                array_push($check_arr, $rubrics);
                $parent_id_level = [Yii::$app->db->getLastInsertID(), $level];
                array_push($pair, $parent_id_level);
            }
        }
    }

    public function load_news($faker)
    {
        $status = ['draft', 'published', 'arhived', 'deleted', 'unpublished'];
        for ($i=1; $i < 500; $i++) {
            if (empty($this->rubrics_ids)){
                $rubrics_id = $this->Rubrics_from_DB();
            }else{
                $rubrics_id = $this->rand_null($this->rand_ids($this->rubrics_ids));
            }
            $this->insert('News', [
                'title' => $faker->text,
                'short_text' => $faker->text,
                'text' => $faker->text,
                'status' => $this->rand_ids($status),
                'alias' => $this->rand_null( $faker->text),
                'meta_description' => $faker->text,
                'meta_keywords' => $faker->text,
                'created_at' => date($format = 'Y-m-d'),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'rubrics_id' => $rubrics_id
            ]);
            array_push($this->news_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_comments($faker)
    {
        $status = ['rejected', 'approved', 'created', 'deleted'];        
        for ($i=1; $i < 800; $i++) {
            $this->insert('Comments', [
                'comment' => $faker->text,
                'status' => array_rand($status),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'news_id' => $this->rand_ids($this->news_ids)
            ]); 
        }
    }

    public function load_waters($faker)
    {
        for ($i=1; $i < 800; $i++) {
            if (empty($this->city_id)) {
                $city_id = $this->City_from_DB();
            }else{
                $city_id = $this->rand_ids($this->city_ids);
            }
            $rand_integral = rand(1,21);
            $lat = rand(1,100) / $rand_integral;
            $lng = rand(1,100) / $rand_integral;
            $this->insert('Waters', [
                'name' => $faker->cityPrefix,
                'lat' => $lat,
                'lng' => $lng,
                'city_id' => $this->rand_null($city_id),
                'founding_date' => $this->rand_null(date($format = 'Y-m-d')),
                'description' => $this->rand_null($faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids))
            ]);
            array_push($this->water_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_competitions($faker)
    {
       $status = ['published','unpublished','delete','arhive'];
       $level = ['international', 'countries',
                 'cities', 'qualifying',
                 'olympic_games', 'continental',
                 'world_cup', 'world_championships'];
        for ($i=1; $i < 100; $i++) {
            if (empty($this->city_id)) {
                $city_id = $this->City_from_DB();
            }else{
                $city_id = $this->rand_ids($this->city_ids);
            }
            $this->insert('Competitions', [
                'name' => $faker->text,
                'city_id' => $this->rand_null($city_id),
                'begin_date' => date($format = 'Y-m-d'),
                'end_date' =>$this->rand_null(date($format = 'Y-m-d')),
                'description' => $this->rand_null($faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'water_id' => $this->rand_ids($this->water_ids),
                'status' => array_rand($status),
                'level' => array_rand($level)
            ]);
            array_push($this->competition_ids, Yii::$app->db->getLastInsertID());
        } 
    }

    public function load_bases($faker)
    {
        for ($i=1; $i < 100; $i++) {
            if (empty($this->city_id)) {
                $city_id = $this->City_from_DB();
            }else{
                $city_id = $this->rand_ids($this->city_ids);
            }
            $this->insert('Bases', [
                'name' => $faker->firstname,                
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'address' => $faker->address,
                'city_id' => $city_id,
                'water_id' => $this->rand_null($this->rand_ids($this->water_ids)),
                'description' => $this->rand_null( $faker->text),
                'phones' => $this->rand_null($faker->phoneNumber),
                'email' => $faker->email,
                'work_time' => $faker->text,
                'date_founding' => date($format = 'Y-m-d'),
                'director' => $this->rand_ids($this->user_ids)
            ]);
            array_push($this->base_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_paddles($faker)
    {
        $state = ['new', 'normal', 'damage', 'critical'];
        $type_paddle = ['scull', 'casement_paddle'];
        for ($i=1; $i < 500; $i++) {
            $weigth = [rand(700, 1100), null];
            $length = [rand(270, 395), null];
            $this->insert('Paddles', [
                'paddle' => $faker->firstname,
                'create_date' => $this->rand_null(date($format = 'Y-m-d')),
                'weigth' => $this->rand_ids($weigth),
                'length' => $this->rand_ids($length),
                'state' => array_rand($state),
                'type_paddle' => array_rand($type_paddle),
                'details' => $this->rand_null( $faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'base_id' => $this->rand_null($this->rand_ids($this->base_ids))
            ]);
            array_push($this->paddles_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_boats($faker)
    {
        $category = ['LM1x','LM2-','LM2x','LM4-','M2+', 'LM4x','LM8+','M1x',
                     'M2-','M2x','M4-','M4+','M4x','M8+','LW1x','LW2x',
                     'LW4x','W1x','W2-','W2x','W4-','W4x','W8+','ASM1x',
                     'ASW1x','LTAMix4+'];

        $state = ['new', 'normal', 'damage', 'critical'];
        $bow_type = ['acute', 'straigth'];
        $reiggers = ['carbon', 'aluminium'];
        for ($i=1; $i < 500; $i++) {
            $this->insert('Boats', [
                'manufacturer' => $faker->company,
                'category' => $this->rand_ids($category),
                'create_date' => $this->rand_null(date($format = 'Y-m-d')),
                'weigth' => $this->rand_null(rand(7, 36)),
                'length' => $this->rand_null(rand(11, 19)),
                'state' => array_rand($state),
                'bow_type' => array_rand($bow_type),
                'details' => $this->rand_null( $faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'reiggers' => array_rand($reiggers),
                'base_id' => $this->rand_null($this->rand_ids($this->base_ids))
            ]);
            array_push($this->boats_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_crews($faker)
    {
        $crew_type = ['LM1x','LM2-','LM2x','LM4-','M2+', 'LM4x','LM8+','M1x',
                      'M2-','M2x','M4-','M4+','M4x','M8+','LW1x','LW2x',
                      'LW4x','W1x','W2-','W2x','W4-','W4x','W8+','ASM1x',
                      'ASW1x','LTAMix4+'];
        $status = ['once_only', 'active', 'inactive', 'awaiting_confirmation', 'Suspended'];
        for ($i=1; $i < 75; $i++) {
            $this->insert('Crews', [
                'slogan' => $this->rand_null( $faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'crew_type' => $this->rand_ids($crew_type),
                'status' => $this->rand_ids($status)
            ]);
            array_push($this->crews_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_races($faker)
    {
        $weather = ['favourable', 'headwind', 'crosswind', 'calm', 'storm'];            
        $status = ['published', 'unpublished',
                   'cancelled', 'waiting',
                   'finished', 'deleted',
                   'arhive'];
        for ($i=1; $i < 50; $i++) {
            $this->insert('Races', [
                'description' => $this->rand_null($faker->text),
                'distance' => rand(500, 20000),
                'start_date' => $this->rand_null(date($format = 'Y-m-d')),
                'heat' => rand(1,11),
                'progression' => $faker->text,
                'weather' => array_rand($weather),
                'status' => array_rand($status),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'competition_id' => $this->rand_null($this->rand_ids($this->competition_ids))
            ]);
            array_push($this->race_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_crews_races()
    {                
        $rand_crews_races = [];
        while (count($rand_crews_races) < 100)
        {
            $crews_race = [$this->rand_ids($this->crews_ids), $this->rand_ids($this->race_ids)];
            if (!in_array($crews_race, $rand_crews_races)) {
                array_push($rand_crews_races, $crews_race);
            }
        }
        foreach ($rand_crews_races as $crews_race)
        {
            $this->insert('Crews_races', [
                'crews_id' => $crews_race[0],
                'race_id' => $crews_race[1],
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'line_number' => rand(1,10),
                'time_2000' => rand(33000, 54000),
                'time_500' => $this->rand_null(rand(33000, 54000)),
                'time_1000' => $this->rand_null(rand(33000, 54000)),
                'time_1500' => $this->rand_null(rand(33000, 54000)),
                'temp_500' => $this->rand_null(rand(29,42)),
                'temp_1000' => $this->rand_null(rand(29,42)),
                'temp_1500' => $this->rand_null(rand(29,42)),
                'temp_2000' => $this->rand_null(rand(29,42)),
                'temp_avg' => $this->rand_null(rand(29,42)),
                'position_500' => $this->rand_null(rand(1,6)),
                'position_1000' => $this->rand_null(rand(1,6)),
                'position_1500' => $this->rand_null(rand(1,6)),
                'position_2000' => rand(1,6)
            ]);
        }      
    }

    public function load_competitions_news()
    {
        $rand_competitions_news = [];
        while(count($rand_competitions_news) < 100)
        {
            $competitions_news = [$this->rand_ids($this->competition_ids), $this->rand_ids($this->news_ids)];
            if (!in_array($competitions_news, $rand_competitions_news)) {
                array_push($rand_competitions_news, $competitions_news);
            }
        }
        foreach ($rand_competitions_news as $competitions_news) {
            $this->insert('Competitions_news', [
                'competition_id' => $competitions_news[0],
                'news_id' => $competitions_news[1]
            ]);
        }
    }

    public function load_crews_persons()
    {
        $rand_crews_persons = [];
        while (count($rand_crews_persons) < 100)
        {
            $crews_persons = [$this->rand_ids($this->crews_ids), $this->rand_ids($this->persons_ids)];
            if (! in_array($crews_persons, $rand_crews_persons)) {
                array_push($rand_crews_persons, $crews_persons);
            }
        }
        foreach ($rand_crews_persons as $crews_persons)
        {
            $this->insert('Crews_persons', [
                'crews_id' => $crews_persons[0],
                'persons_id' => $crews_persons[1]
            ]);            
        }
    }

    public function load_base_persons ()
    {
        $rand_base_persons = [];
        while (count($rand_base_persons) < 100) {
            $base_persons = [$this->rand_ids($this->base_ids), $this->rand_ids($this->persons_ids)];
            if (!in_array($base_persons, $rand_base_persons)) {
                array_push($rand_base_persons, $base_persons);
            }
        }
        foreach ($rand_base_persons as $base_persons ) {
            $this->insert('Base_persons', [
                'base_id' => $base_persons[0],
                'persons_id' => $base_persons[1]
            ]);
        }
    }

    public function load_crews_boats ()
    {
        $rand_crews_boats = [];
        while (count($rand_crews_boats) < 100) {
            $crews_boats = [$this->rand_ids($this->crews_ids), $this->rand_ids($this->boats_ids)];
            if (!in_array($crews_boats, $rand_crews_boats)) {
                array_push($rand_crews_boats, $crews_boats);
            }
        }
        foreach ($rand_crews_boats as $crews_boats ) {
            $this->insert('Crews_boats', [
                'crews_id' => $crews_boats[0],
                'boats_id' => $crews_boats[1]
            ]);
        }
    }

    public function load_crews_paddles ()
    {
        $rand_crews_paddles = [];
        while (count($rand_crews_paddles) < 100) {
            $base_persons = [$this->rand_ids($this->crews_ids), $this->rand_ids($this->paddles_ids)];
            if (!in_array($base_persons, $rand_crews_paddles)) {
                array_push($rand_crews_paddles, $base_persons);
            }
        }
        foreach ($rand_crews_paddles as $base_persons ) {
            $this->insert('Crews_paddles', [
                'crews_id' => $base_persons[0],
                'paddles_id' => $base_persons[1]          
            ]);
        }
    }

    public function load_persons_type ($faker)
    {
        for ($i=1; $i < 150; $i++) {             
            $this->insert('Persons_type', [
                'name' => $faker->firstname,
                'description' => $this->rand_null($faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids))
            ]);
            array_push($this->persons_type_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_persons_type_persons()
    {
        $rand_persons_type_persons = [];
        while (count($rand_persons_type_persons) < 175) {
            $persons_type_persons = [$this->rand_ids($this->persons_type_ids), $this->rand_ids($this->persons_ids)];
            if (!in_array($persons_type_persons, $rand_persons_type_persons)) {
                array_push($rand_persons_type_persons, $persons_type_persons);
            }
        }
        foreach ($rand_persons_type_persons as $persons_type_persons ) {
            $this->insert('Persons_type_persons', [
                'persons_type_id' => $persons_type_persons[0],
                'persons_id' => $persons_type_persons[1]
            ]);
        }
    }

    public function load_training($faker)
    {
        $health = ['good', 'normal', 'bad'];
        $training_type = ['command','personal','command-personal'];
        for ($i=1; $i < 1000; $i++) {
            $this->insert('Training', [
                'health' => array_rand($health),
                'training_type' => array_rand($training_type),
                'code' => $faker->text,
                'date' => date($format = 'Y-m-d'),
                'description' => $this->rand_null($faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids))
            ]);
            array_push($this->training_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_results_type($faker)
    {
        for ($i=1; $i < 100; $i++) {
            $this->insert('Results_type', [
                'name' => $faker->firstname,
                'formula' => $this->rand_null( $faker->text),
                'description' => $this->rand_null( $faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids))
            ]);
            array_push($this->results_type_ids,  Yii::$app->db->getLastInsertID());
        }
    }

    public function load_fields_type($faker)
    {
        $type = ['weight', 'length', 'width', 'height',
                 'distance', 'time', 'the_temp', 'pulse',
                 'blood_pressure', 'watts', 'calories', 'the_approach',
                 'the_repetition', 'the_angle_of_inclination', 'lactate'];
                 
        for ($i=1; $i < 1000; $i++) {
            $this->insert('Fields_type', [
                'name' => $faker->firstname,
                'details' => $this->rand_null($faker->text),
                'type' => array_rand($type),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'results_type_id' => $this->rand_ids($this->results_type_ids)
            ]);
            array_push($this->fields_type_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_training_results()
    {
        $training_results = [];
        while (count($training_results) < 50) {            
            $rand = [$this->rand_ids($this->results_type_ids), $this->rand_ids($this->training_ids)];
            if (!in_array($rand, $training_results)) {
                array_push($training_results, $rand);
            }
        }
        foreach ($training_results as $train_result ) {
            $this->insert('Training_results', [
                'results_type_id' => $train_result[0],
                'training_id' => $train_result[1]
            ]);
            array_push($this->training_results_ids, Yii::$app->db->getLastInsertID());
        }
    }

    public function load_results_fields($faker)
    {
        $results_fields = [];
        while (count($results_fields) < 50) {
            $rand = [$this->rand_ids($this->fields_type_ids), $this->rand_ids($this->training_results_ids)];
            if (!in_array($rand, $results_fields)) {
                array_push($results_fields, $rand);
            }
        }

        foreach($results_fields as $fields_type_training_results) {
            $this->insert('Results_fields', [
                'value' => rand(1000, 10000),
                'description' => $this->rand_null($faker->text),
                'created_at' => date($format = 'Y-m-d'),
                'updated_at' => $this->rand_null(date($format = 'Y-m-d')),
                'created_by' => $this->rand_ids($this->user_ids),
                'updated_by' => $this->rand_null($this->rand_ids($this->user_ids)),
                'fields_type_id' => $fields_type_training_results[0],
                'training_results_id' => $fields_type_training_results[1]
            ]);
        }
    }

    public function load_persons_training()
        {
            $persons_training = [];
            while (count($persons_training) < 20) {
                $rand_persons_training = [$this->rand_ids($this->persons_ids), $this->rand_ids($this->training_ids)];
                if (!in_array($rand_persons_training, $persons_training)) {
                    array_push($persons_training, $rand_persons_training);
                }
            }
            foreach ($persons_training as $persons_training) {
                $this->insert('Persons_training', [
                    'persons_id' => $persons_training[0],
                    'training_id' => $persons_training[1]
                ]);
            }
        }

    public function down()
    {
        $this->delete('Results_fields');
        $this->delete('Training_results');        
        $this->delete('Persons_training');
        $this->delete('Fields_type');
        $this->delete('Results_type');
        $this->delete('Training');
        $this->delete('Persons_type_persons');
        $this->delete('Persons_type');
        $this->delete('Crews_paddles');
        $this->delete('Crews_boats');
        $this->delete('Base_persons');
        $this->delete('Crews_persons');
        $this->delete('Competitions_news');
        $this->delete('Crews_races');
        $this->delete('Races');
        $this->delete('Crews');
        $this->delete('Boats');
        $this->delete('Paddles');
        $this->delete('Bases');
        $this->delete('Competitions');  
        $this->delete('Waters');  
        $this->delete('Comments');
        $this->delete('News');   
        $this->delete('Rubrics');
        $this->delete('Persons');
        $this->delete('City');
        $this->delete('Countries');
        $this->delete('Users');
    }
}
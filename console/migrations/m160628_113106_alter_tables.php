<?php

use yii\db\Migration;

class m160628_113106_alter_tables extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%Waters}}','city_id','integer NULL');
        $this->alterColumn('{{%News}}','rubrics_id','integer NULL');
        $this->alterColumn('{{%Competitions}}','city_id','integer NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%Waters}}','city_id','integer NOT NULL');
        $this->alterColumn('{{%News}}','rubrics_id','integer NOT NULL');
        $this->alterColumn('{{%Competitions}}','city_id','integer NOT NULL');
    }
}

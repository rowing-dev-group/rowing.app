<?php

use yii\db\Migration;

class m160418_205049_Persons_table extends Migration
{
     public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Persons}}', [
            'persons_id' =>$this->primaryKey(),
            'coach_id' =>$this->integer(),
            'about' =>$this->string(4096),
            'city_id' =>$this->integer()->notNull(),
            'begin_carier' =>$this->dateTime()->notNull(),
            'end_carier' =>$this->dateTime(),
            'social_accounts' =>$this->string(5120),            
            'body_type' =>"enum('ectomorph', 'endomorph', 'mesomorph')",
            'personal_life' =>$this->string(2048),
            'place_birthday' =>$this->integer(),
            'phones' =>$this->string(3072),
            'status' => "enum('active', 'inactive', 'reabilitation'".
                            ")NOT NULL DEFAULT 'active'"
        ],$tableOptions);            
    }

    public function safeDown()
    {
        $this->dropTable('{{%Persons}}');
    }
}

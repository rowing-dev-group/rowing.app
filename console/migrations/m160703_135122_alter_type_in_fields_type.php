<?php

use yii\db\Migration;

class m160703_135122_alter_type_in_fields_type extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%Fields_type}}','type', "enum(
                           'weight', 'length', 'width', 'height',
                           'distance', 'time', 'the_temp', 'pulse',
                           'blood_pressure', 'watts', 'calories', 'the_approach',
                           'the_repetition', 'the_angle_of_inclination', 'lactate') NULL");
     
    }

    public function down()
    {
       $this->alterColumn('{{%Fields_type}}','type', "enum('DOUBLE', 'TIME')");
    }
}

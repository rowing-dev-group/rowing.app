<?php

use yii\db\Migration;

class m130524_201442_Users_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Users}}', [  
            'user_id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string(255)->notNull()->unique(),            
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'persons_id' =>$this->integer(),
            'firstname' =>$this->string(255)->notNull(),
            'lastname' =>$this->string(255)->notNull(), 
            'middlename' =>$this->string(255),
            'birthday'=> $this->dateTime(),
            'status' => "enum('active','inactive','deleted'".
                             ") NOT NULL DEFAULT 'active'"            
        ], $tableOptions);       
    }

    public function down()
    {
        $this->dropTable('{{%Users}}');
    }
}

<?php
 
use yii\db\Migration;

class m160420_222654_Comments_table extends Migration
{
    public function up()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {        
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%Comments}}', [  
            'comments_id' => $this->primaryKey(),
            'comment' => $this->string(4096)->notNull(),
            'status' => "enum('rejected', 'approved', 'created','deleted'".
                              ")NOT NULL DEFAULT 'created'",
            'email' => $this->string(255),
            'created_at' => $this->dateTime()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_at' => $this->dateTime(),
            'updated_by' =>$this->integer(),        
            'news_id' => $this->integer()->notNull()
        ], $tableOptions);  
    }    
    
    public function down()
    {
        $this->dropTable('{{%Comments}}');
    }        
}